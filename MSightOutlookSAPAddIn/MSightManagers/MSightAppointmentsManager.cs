﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;
using System.Windows.Forms;
using MSightOutlookSAPAddIn.UtilClasses;
using MSightOutlookSAPAddIn.SAPWebServices;
using MSightOutlookSAPAddIn.CustomControls;

namespace MSightOutlookSAPAddIn.MSightManagers
{
    public class MSightAppointmentsManager
    {

        public static void RefreshAppointments()
        {
            try
            {                
                SettingsStruct _SettingsStruct = MSightRegistryManager.LoadSettingsFromRegistry();
                int SyncWithSAPDaysVal = int.Parse(_SettingsStruct.SyncWithSAPDays.ToString());
                SyncWithSAPDaysVal = SyncWithSAPDaysVal * -1;

                List<SAPQueryAppointment> SAPQueryAppointmentList = SAPQueryAppointmentManager.QueryAppointmentsFromSAP(
                                                                                                                        Globals.ThisAddIn.InternalID, 
                                                                                                                        DateTime.Now.AddDays(SyncWithSAPDaysVal),
                                                                                                                        DateTime.Now);

                foreach (SAPQueryAppointment SAPQueryAppointmentItem in SAPQueryAppointmentList)
                {
                    SAPQueryAppointmentParty EmpResID = SAPQueryAppointmentItem.SAPQueryAppointmentPartyList.Find(x => x.PartyType == PartyType.EmployeeResponsible && x.PartyID == Globals.ThisAddIn.InternalID);

                    if (EmpResID.PartyID != null &&  EmpResID.PartyID.Equals(Globals.ThisAddIn.InternalID))      // In Case of Employee Responseible Party Create Appointments
                        MSightAppointmentsManager.SearchAndCreateAppointment(SAPQueryAppointmentItem);
                    else
                    {
                        SAPQueryAppointmentParty SalesRepID = SAPQueryAppointmentItem.SAPQueryAppointmentPartyList.Find(x => x.PartyType == PartyType.Attendee && x.PartyID == Globals.ThisAddIn.InternalID);
                        if (SalesRepID.PartyID != null && SalesRepID.PartyID.Equals(Globals.ThisAddIn.InternalID))
                        {
                            MSightAppointmentsManager.SearchAndCreateAppointment(SAPQueryAppointmentItem);
                        }
                    }

                }

                var ribbon = (MSightOutlookRibbon)Globals.Ribbons.GetRibbon(typeof(MSightOutlookRibbon));
                if (ribbon != null)
                    ribbon.LastSyncedDateTime.Label = DateTime.Now.ToString("MM.dd.yyyy hh:mm tt");

            }
            catch(System.Exception ex)
            {
                MessageBox.Show("The following error occurred while Processing and Creating Appointment in Outlook: " + ex.Message);
            }
        }


        public static void SearchAndCreateAppointment(SAPQueryAppointment _AppointmentStructObj)
        {
            Boolean isFound = false;
            MSightAppointmentsManager _MSightAppointmentsManagerObj = new MSightAppointmentsManager();
            
            SettingsStruct _SettingsStruct = MSightRegistryManager.LoadSettingsFromRegistry();
            int SyncWithSAPDaysVal = int.Parse(_SettingsStruct.SyncWithSAPDays.ToString());
            SyncWithSAPDaysVal = SyncWithSAPDaysVal * -1;

            // Get all Apointments of previous three days to search for the appointments
            Items AppointmentsList = _MSightAppointmentsManagerObj.GetAllAppointments(DateTime.Now.AddDays(SyncWithSAPDaysVal), DateTime.Now.AddWorkDays(15));
            foreach (AppointmentItem item in AppointmentsList)
            {
                UserProperty userProp = item.UserProperties.Find("SAPAppointmentOID", true);
                String SAPAppointmentOID = userProp.Value;
                if (SAPAppointmentOID.Equals(_AppointmentStructObj.AppointmentID))
                {
                    isFound = true;
                    break;
                }
            }

            if (isFound == false)
            {
                _MSightAppointmentsManagerObj.CreateAppointment(_AppointmentStructObj);
            }
        }

        private void CreateAppointment(SAPQueryAppointment _AppointmentStructObj)
        {
            try
            {
                var MyOutlookAddIn = MSightOutlookSAPAddIn.Globals.ThisAddIn;
                SettingsStruct _SettingsStruct = MSightRegistryManager.LoadSettingsFromRegistry();
                int futuredays = int.Parse(_SettingsStruct.AppointmentWorkingDays.ToString());

                QueryCustomerQuoteInManager _QueryCustomerQuoteInManager = new QueryCustomerQuoteInManager();
                String SalesQuoteID = "";
                if (_AppointmentStructObj.Name.Contains("#") && _AppointmentStructObj.Name.Contains("$"))
                    SalesQuoteID = _AppointmentStructObj.Name.Substring(_AppointmentStructObj.Name.IndexOf("#") + 2, _AppointmentStructObj.Name.IndexOf("$") - 4).Trim();
                else
                    return;

                SAPCustomerQuote _SAPCustomerQuoteObj = _QueryCustomerQuoteInManager.QueryCustomerQuoteInFromSAP(SalesQuoteID);

                if (_SAPCustomerQuoteObj.SalesCycle.Equals("Transactional"))
                    futuredays = 2;
                else if (_SAPCustomerQuoteObj.SalesCycle.Equals("Stock"))
                    futuredays = 7;
                else if (_SAPCustomerQuoteObj.SalesCycle.Equals("Project"))
                    futuredays = 14;

                AppointmentItem newAppointment = (AppointmentItem)MyOutlookAddIn.Application.CreateItem(OlItemType.olAppointmentItem);
                if (_SAPCustomerQuoteObj != null && _SAPCustomerQuoteObj.PostingDate != DateTime.MinValue)
                {
                    newAppointment.Start = _SAPCustomerQuoteObj.PostingDate.AddWorkDays(futuredays);
                    newAppointment.End = _SAPCustomerQuoteObj.PostingDate.AddWorkDays(futuredays);
                }
                else
                {
                    newAppointment.Start = _AppointmentStructObj.ScheduledPeriod_StartDateTime.AddWorkDays(futuredays);
                    newAppointment.End = _AppointmentStructObj.ScheduledPeriod_StartDateTime.AddWorkDays(futuredays);
                }
                
                newAppointment.Location = _AppointmentStructObj.MeetingPlace;
                newAppointment.Body = _AppointmentStructObj.Notes;
                newAppointment.AllDayEvent = _AppointmentStructObj.FullDayIndicator;
                newAppointment.Subject = _AppointmentStructObj.Name;
                newAppointment.ReminderSet = true;

                TimeSpan span = newAppointment.Start.Subtract(DateTime.Now);
                newAppointment.ReminderMinutesBeforeStart = int.Parse(Math.Round(span.TotalMinutes).ToString());

                if (_SAPCustomerQuoteObj.SalesRep != null && _SAPCustomerQuoteObj.SalesRep_EmailAddress != null)
                {
                    ContactInfo _ContactInfo = new ContactInfo();
                    _ContactInfo.FirstName = _SAPCustomerQuoteObj.SalesRep_FamilyName;
                    _ContactInfo.LastName = _SAPCustomerQuoteObj.SalesRep_GivenName;
                    _ContactInfo.EmailAddress = _SAPCustomerQuoteObj.SalesRep_EmailAddress;
                    _ContactInfo.InternalID = _SAPCustomerQuoteObj.SalesRep;

                    if (MSightContactsManager.FindContactByName(_ContactInfo.FirstName, _ContactInfo.LastName) == false)
                        MSightContactsManager.AddContact(_ContactInfo);
                }

                Recipients sentTo = newAppointment.Recipients;
                Recipient recipient = null;
                foreach (SAPQueryAppointmentParty SAPQueryAppointmentParty in _AppointmentStructObj.SAPQueryAppointmentPartyList)
                {
                    ContactInfo ContactInfoObj = SAPQueryAppointmentParty.PartyContactInfo;
                    if (MSightContactsManager.FindContactByName(ContactInfoObj.FirstName, ContactInfoObj.LastName) == false)
                        MSightContactsManager.AddContact(ContactInfoObj);

                    

                    if (String.IsNullOrEmpty(ContactInfoObj.EmailAddress))
                    {
                        if (!String.IsNullOrEmpty(ContactInfoObj.FirstName) && !String.IsNullOrEmpty(ContactInfoObj.LastName))
                        {
                            MessageBox.Show(String.Format("Email Address for the Business Partner/Employee '{0} {1}' doesn't exists in SAP, Please add email in SAP for future appoitnments",
                               ContactInfoObj.FirstName, ContactInfoObj.LastName),
                               "Email Address not exits in SAP", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                        }
                    }
                    else
                    {
                        recipient = newAppointment.Recipients.Add(ContactInfoObj.EmailAddress);
                        switch (SAPQueryAppointmentParty.PartyType)
                        {
                            case PartyType.Organizer:
                                recipient.Type = (int)OlMeetingRecipientType.olOrganizer;
                                break;
                            case PartyType.EmployeeResponsible:
                                recipient.Type = (int)OlMeetingRecipientType.olOrganizer;
                                break;
                            case PartyType.Account:
                                recipient.Type = (int)OlMeetingRecipientType.olRequired;
                                break;
                            case PartyType.Attendee:
                                recipient.Type = (int)OlMeetingRecipientType.olRequired;
                                break;
                        }
                    }
                }

                sentTo.ResolveAll();
                UserProperty Property = newAppointment.UserProperties.Add("SAPAppointmentOID", OlUserPropertyType.olText);
                Property.Value = _AppointmentStructObj.AppointmentID;
                newAppointment.Save();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("The following error occurred while creating Appointment: " + ex.Message);
            }
        }

        private Items GetAllAppointments(DateTime StartDateTime, DateTime EndDateTime)
        {
            var MyOutlookAddIn = MSightOutlookSAPAddIn.Globals.ThisAddIn;
            Folder folder = MyOutlookAddIn.Application.Session.GetDefaultFolder(OlDefaultFolders.olFolderCalendar) as Folder;
            string filter = string.Format("[Start] >= '{0:g}' AND [End] <= '{1:g}'", StartDateTime, EndDateTime);
            try
            {
                Items calItems = folder.Items;
                calItems.IncludeRecurrences = true;
                calItems.Sort("[Start]", Type.Missing);
                Items restrictItems = calItems.Restrict(filter);
                if (restrictItems.Count > 0)
                    return restrictItems;
                else
                    return null;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("The following error occurred while getting all Appointments: " + ex.Message);
                return null;
            }
        }
    }


    public static class DateTimeExtensions
    {
        public static DateTime AddWorkDays(this DateTime date, int workingDays)
        {
            int direction = workingDays < 0 ? -1 : 1;
            DateTime newDate = date;
            while (workingDays != 0)
            {
                newDate = newDate.AddDays(direction);
                if (newDate.DayOfWeek != DayOfWeek.Saturday &&
                    newDate.DayOfWeek != DayOfWeek.Sunday &&
                    !newDate.IsHoliday())
                {
                    workingDays -= direction;
                }
            }
            return newDate;
        }

        public static bool IsHoliday(this DateTime date)
        {
            // You'd load/cache from a DB or file somewhere rather than hardcode
            DateTime[] holidays =
            new DateTime[] { 
                              new DateTime(DateTime.Now.Year,01,01)//,
                              //new DateTime(DateTime.Now.Year,01,18),
                              //new DateTime(DateTime.Now.Year,05,30),
                              //new DateTime(DateTime.Now.Year,09,05),
                              //new DateTime(DateTime.Now.Year,11,11),
                              //new DateTime(DateTime.Now.Year,11,24),
                              //new DateTime(DateTime.Now.Year,12,25),
                              //new DateTime(DateTime.Now.Year,12,26)
                           };
            return holidays.Contains(date.Date);
        }
    }

}
