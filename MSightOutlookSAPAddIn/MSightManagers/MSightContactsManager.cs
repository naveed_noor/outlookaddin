﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;
using System.Windows.Forms;
using MSightOutlookSAPAddIn.UtilClasses;

namespace MSightOutlookSAPAddIn.MSightManagers
{
    public class MSightContactsManager
    {
        public static Boolean FindContactByName(string FirstName, string LastName)
        {
            var MyOutlookAddIn = MSightOutlookSAPAddIn.Globals.ThisAddIn;
            NameSpace outlookNameSpace = MyOutlookAddIn.Application.GetNamespace("MAPI");
            MAPIFolder contactsFolder = outlookNameSpace.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderContacts);
            Items contactItems = contactsFolder.Items;
            try
            {
                ContactItem contact = (ContactItem)contactItems.Find(String.Format("[FirstName]='{0}' and " + "[LastName]='{1}'", FirstName != null ? FirstName.Trim() : "", LastName != null ? LastName.Trim() : ""));
                if (contact != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("The following error occurred while searching contact: " + ex.Message);
                return false;
            }
        }


        public static void AddContact(ContactInfo _ContactInfoObj)
        {
            if (String.IsNullOrEmpty(_ContactInfoObj.FirstName) && String.IsNullOrEmpty(_ContactInfoObj.LastName))
                return;

            var MyOutlookAddIn = MSightOutlookSAPAddIn.Globals.ThisAddIn;
            ContactItem newContact = (ContactItem)MyOutlookAddIn.Application.CreateItem(OlItemType.olContactItem);
            try
            {
                newContact.FirstName = _ContactInfoObj.FirstName != null ? _ContactInfoObj.FirstName.Trim() : "";
                newContact.LastName = _ContactInfoObj.LastName != null ? _ContactInfoObj.LastName.Trim() : "";
                newContact.Email1Address = _ContactInfoObj.EmailAddress != null ? _ContactInfoObj.EmailAddress.Trim() : "";
                newContact.CustomerID = _ContactInfoObj.InternalID;
                newContact.PrimaryTelephoneNumber = _ContactInfoObj.PhoneNo;

                newContact.MailingAddressStreet = _ContactInfoObj.StreetPrefixName;
                newContact.MailingAddressCity = _ContactInfoObj.CityName;
                newContact.MailingAddressState = _ContactInfoObj.RegionCode;
                newContact.MailingAddressPostalCode = _ContactInfoObj.StreetPostalCode;
                newContact.MailingAddressCountry = _ContactInfoObj.CountryCode;

                newContact.Save();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("The following error occurred while adding new Contact: " + ex.Message);
            }
        }

    }
}
