﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MSightOutlookSAPAddIn.SAPWebServices;
using MSightOutlookSAPAddIn.UtilClasses;

namespace MSightOutlookSAPAddIn.CustomControls
{
    public partial class SAPEmployeeLinkControl : UserControl
    {
        public SAPEmployeeLinkControl()
        {
            InitializeComponent();
            String InternalID, FirstName, LastName, Email;
            Boolean res = MaintainLinkedEmployeeCredentials.ReadEmployeeCredentialsFromRegistry(out InternalID, out FirstName, out LastName, out Email);
            if (res == true)
            {
                this.InternalID.Text = InternalID;
                this.FirstName.Text = FirstName;
                this.LastName.Text = LastName;
                this.Email.Text = Email;
            }
        }

        List<ContactInfo> _BusinessPartnersInfoList;

        private void LoadListFromSAPBtn_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            _BusinessPartnersInfoList = QueryBusinessPartners.GetBusinessPartnersInfoFromSAP("*", BusinessParnerCategoryCode.Employee);
            _BusinessPartnersInfoList = _BusinessPartnersInfoList.OrderBy(o => o.FirstName).ToList<ContactInfo>();

            List<String> list = new List<string>();
            foreach (var item in _BusinessPartnersInfoList)
            {
                list.Add(String.Format("{0} {1} {2}", item.FirstName , item.LastName, item.EmailAddress));
            }
            this.SAPBusinessPartnersComboBox.DataSource = list;
            if (list.Count > 0)
                MessageBox.Show("Employees/Business Parners list loaded sucessfully......");
            else
                MessageBox.Show("Employees/Business Parners list Not loaded.");

            Cursor.Current = Cursors.Default;
        }

        private void SAPBusinessPartnersComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ContactInfo _BusinessPartnersInfoObj = _BusinessPartnersInfoList[this.SAPBusinessPartnersComboBox.SelectedIndex];
            this.InternalID.Text = _BusinessPartnersInfoObj.InternalID;
            this.FirstName.Text = _BusinessPartnersInfoObj.FirstName;
            this.LastName.Text = _BusinessPartnersInfoObj.LastName;
            this.Email.Text = _BusinessPartnersInfoObj.EmailAddress;
        }

        private void SaveEmployeeLinkBtn_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MaintainLinkedEmployeeCredentials.StoreEmployeeCredentialsInRegistry(this.InternalID.Text, this.FirstName.Text, this.LastName.Text, this.Email.Text);
            Globals.ThisAddIn.LoadUserSettingsFromRegistry();
            Cursor.Current = Cursors.Default;
        }

    }
}
