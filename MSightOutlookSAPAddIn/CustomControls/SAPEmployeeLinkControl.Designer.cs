﻿namespace MSightOutlookSAPAddIn.CustomControls
{
    partial class SAPEmployeeLinkControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SAPBusinessPartnersComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Email = new System.Windows.Forms.Label();
            this.LastName = new System.Windows.Forms.Label();
            this.FirstName = new System.Windows.Forms.Label();
            this.InternalID = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SaveEmployeeLinkBtn = new System.Windows.Forms.Button();
            this.LoadListFromSAPBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SAPBusinessPartnersComboBox
            // 
            this.SAPBusinessPartnersComboBox.FormattingEnabled = true;
            this.SAPBusinessPartnersComboBox.Location = new System.Drawing.Point(7, 106);
            this.SAPBusinessPartnersComboBox.Name = "SAPBusinessPartnersComboBox";
            this.SAPBusinessPartnersComboBox.Size = new System.Drawing.Size(264, 21);
            this.SAPBusinessPartnersComboBox.TabIndex = 0;
            this.SAPBusinessPartnersComboBox.SelectedIndexChanged += new System.EventHandler(this.SAPBusinessPartnersComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select your SAP employee name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 171);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Internal ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "First Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Last Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 258);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Email";
            // 
            // Email
            // 
            this.Email.Location = new System.Drawing.Point(92, 254);
            this.Email.Name = "Email";
            this.Email.Size = new System.Drawing.Size(182, 21);
            this.Email.TabIndex = 11;
            // 
            // LastName
            // 
            this.LastName.Location = new System.Drawing.Point(92, 225);
            this.LastName.Name = "LastName";
            this.LastName.Size = new System.Drawing.Size(182, 21);
            this.LastName.TabIndex = 10;
            // 
            // FirstName
            // 
            this.FirstName.Location = new System.Drawing.Point(92, 196);
            this.FirstName.Name = "FirstName";
            this.FirstName.Size = new System.Drawing.Size(182, 21);
            this.FirstName.TabIndex = 9;
            // 
            // InternalID
            // 
            this.InternalID.Location = new System.Drawing.Point(92, 167);
            this.InternalID.Name = "InternalID";
            this.InternalID.Size = new System.Drawing.Size(182, 21);
            this.InternalID.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(28, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(224, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Selected Employee Information";
            // 
            // SaveEmployeeLinkBtn
            // 
            this.SaveEmployeeLinkBtn.Image = global::MSightOutlookSAPAddIn.Properties.Resources.save_sap;
            this.SaveEmployeeLinkBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SaveEmployeeLinkBtn.Location = new System.Drawing.Point(92, 292);
            this.SaveEmployeeLinkBtn.Name = "SaveEmployeeLinkBtn";
            this.SaveEmployeeLinkBtn.Size = new System.Drawing.Size(96, 47);
            this.SaveEmployeeLinkBtn.TabIndex = 2;
            this.SaveEmployeeLinkBtn.Text = "Save";
            this.SaveEmployeeLinkBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SaveEmployeeLinkBtn.UseVisualStyleBackColor = true;
            this.SaveEmployeeLinkBtn.Click += new System.EventHandler(this.SaveEmployeeLinkBtn_Click);
            // 
            // LoadListFromSAPBtn
            // 
            this.LoadListFromSAPBtn.Image = global::MSightOutlookSAPAddIn.Properties.Resources.query_projects;
            this.LoadListFromSAPBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LoadListFromSAPBtn.Location = new System.Drawing.Point(55, 13);
            this.LoadListFromSAPBtn.Name = "LoadListFromSAPBtn";
            this.LoadListFromSAPBtn.Size = new System.Drawing.Size(171, 62);
            this.LoadListFromSAPBtn.TabIndex = 3;
            this.LoadListFromSAPBtn.Text = "Load List From SAP";
            this.LoadListFromSAPBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LoadListFromSAPBtn.UseVisualStyleBackColor = true;
            this.LoadListFromSAPBtn.Click += new System.EventHandler(this.LoadListFromSAPBtn_Click);
            // 
            // SAPEmployeeLinkControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Email);
            this.Controls.Add(this.LastName);
            this.Controls.Add(this.FirstName);
            this.Controls.Add(this.InternalID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LoadListFromSAPBtn);
            this.Controls.Add(this.SaveEmployeeLinkBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SAPBusinessPartnersComboBox);
            this.Name = "SAPEmployeeLinkControl";
            this.Size = new System.Drawing.Size(287, 344);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox SAPBusinessPartnersComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SaveEmployeeLinkBtn;
        private System.Windows.Forms.Button LoadListFromSAPBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Email;
        private System.Windows.Forms.Label LastName;
        private System.Windows.Forms.Label FirstName;
        private System.Windows.Forms.Label InternalID;
        private System.Windows.Forms.Label label6;
    }
}
