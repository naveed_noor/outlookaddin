﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using MSightOutlookSAPAddIn.CustomControls;
using Microsoft.Office.Tools;
using MSightOutlookSAPAddIn.UtilClasses;
using MSightOutlookSAPAddIn.MSightManagers;
using MSightOutlookSAPAddIn.SAPWebServices;
using System.Windows.Forms;

namespace MSightOutlookSAPAddIn.CustomControls
{
    public partial class MSightOutlookRibbon
    {
        private void MSightOutlookRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            if (MSightOutlookSAPAddIn.Globals.ThisAddIn != null)
            if (MSightOutlookSAPAddIn.Globals.ThisAddIn.IsLoggedIntoSAP == true && MSightOutlookSAPAddIn.Globals.ThisAddIn.IsEmployeeResponsibleLinked == true)
            {
                SettingsStruct _SettingsStruct = MSightRegistryManager.LoadSettingsFromRegistry();
                this.IsSyncingWithSAPChkbox.Checked = _SettingsStruct.EnableStatus;
            }
        }

        private SAPLogonControl _SAPLogonControl = new SAPLogonControl();
        private SAPEmployeeLinkControl _SAPEmployeeLinkControl = new SAPEmployeeLinkControl();
        private MSightAddInSettings _MSightAddInSettings = new MSightAddInSettings();

        private Boolean isAlreadyAdded = false;

        private void LogonButton_Click(object sender, RibbonControlEventArgs e)
        {
            var MyOutlookAddIn = MSightOutlookSAPAddIn.Globals.ThisAddIn;
            if (isAlreadyAdded == false)
            {
                MyOutlookAddIn.UserLoginCustomControlPane = MyOutlookAddIn.CustomTaskPanes.Add(_SAPLogonControl, "Logon to SAP ");
                MyOutlookAddIn.UserLoginCustomControlPane.Width = 250;
                isAlreadyAdded = true;
            }
            if (MyOutlookAddIn.SAPEmployeeLinkControl != null && MyOutlookAddIn.SAPEmployeeLinkControl.Visible == true)
                MyOutlookAddIn.SAPEmployeeLinkControl.Visible = false;
            if (MyOutlookAddIn.MSightSettingsTaskPane != null && MyOutlookAddIn.MSightSettingsTaskPane.Visible == true)
                MyOutlookAddIn.MSightSettingsTaskPane.Visible = false;

            MyOutlookAddIn.UserLoginCustomControlPane.Visible = true;
        }

        private void SyncAppointmentsButton_Click(object sender, RibbonControlEventArgs e)
        {
            if (Globals.ThisAddIn._isRunning == false)
            {
                if (MessageBox.Show("Are you sure to sync Appointments from SAP ?", "Sync Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    Globals.ThisAddIn._isRunning = true;
                    MSightAppointmentsManager.RefreshAppointments();
                    Globals.ThisAddIn._isRunning = false;
                    Cursor.Current = Cursors.Default;
                }
            }
            else
                MessageBox.Show("Sync with SAP is already in Progress...... Please wait for some time.", "already working", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        bool isSAPEmployeesListAlreadyAdded = false;
        private void SAPEmployeeLinkBtn_Click(object sender, RibbonControlEventArgs e)
        {
            var MyOutlookAddIn = MSightOutlookSAPAddIn.Globals.ThisAddIn;            

            if (isSAPEmployeesListAlreadyAdded == false)
            {
                MyOutlookAddIn.SAPEmployeeLinkControl = MyOutlookAddIn.CustomTaskPanes.Add(_SAPEmployeeLinkControl, "SAP Employees List");
                MyOutlookAddIn.SAPEmployeeLinkControl.Width = 300;
                isSAPEmployeesListAlreadyAdded = true;
            }
            if (MyOutlookAddIn.UserLoginCustomControlPane != null && MyOutlookAddIn.UserLoginCustomControlPane.Visible == true)
                MyOutlookAddIn.UserLoginCustomControlPane.Visible = false;

            if (MyOutlookAddIn.MSightSettingsTaskPane != null && MyOutlookAddIn.MSightSettingsTaskPane.Visible == true)
                MyOutlookAddIn.MSightSettingsTaskPane.Visible = false;


            MyOutlookAddIn.SAPEmployeeLinkControl.Width = 300;
            MyOutlookAddIn.SAPEmployeeLinkControl.Visible = true;

        }

        private void SettingsBtn_Click(object sender, RibbonControlEventArgs e)
        {
            var MyOutlookAddIn = MSightOutlookSAPAddIn.Globals.ThisAddIn;
           
            if (MyOutlookAddIn.UserLoginCustomControlPane != null && MyOutlookAddIn.UserLoginCustomControlPane.Visible == true)
                MyOutlookAddIn.UserLoginCustomControlPane.Visible = false;

            if (MyOutlookAddIn.SAPEmployeeLinkControl != null && MyOutlookAddIn.SAPEmployeeLinkControl.Visible == true)
                MyOutlookAddIn.SAPEmployeeLinkControl.Visible = false;

            MyOutlookAddIn.MSightSettingsTaskPane = MyOutlookAddIn.CustomTaskPanes.Add(_MSightAddInSettings, "Mind Sight AddIn Settings");
            MyOutlookAddIn.MSightSettingsTaskPane.Width = 400;
            MyOutlookAddIn.MSightSettingsTaskPane.Visible = true;
        }
    }
}
