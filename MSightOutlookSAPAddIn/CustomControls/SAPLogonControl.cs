﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MSightOutlookSAPAddIn.SAPWebServices;
using MSightOutlookSAPAddIn.UtilClasses;

namespace MSightOutlookSAPAddIn.CustomControls
{
    public partial class SAPLogonControl : UserControl
    {
        public SAPLogonControl()
        {
            InitializeComponent();
           
        }

        private void LogonBtn_Click(object sender, EventArgs e)
        {
            if (this.SAPTenantIDTxt.Text.Equals(""))
            {
                MessageBox.Show(AppConstants.Enter_SAP_Tenant_ID, "Tanent ID Required", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (UsernameTxt.Text.Equals(""))
            {
                MessageBox.Show(AppConstants.Enter_User_Name, "Username Required", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if(PasswordTxt.Text.Equals(""))
            {
                MessageBox.Show(AppConstants.Enter_Password, "Password Required", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            Cursor.Current = Cursors.WaitCursor;

            Globals.ThisAddIn.SetUserCredentials(SAPTenantIDTxt.Text, UsernameTxt.Text, PasswordTxt.Text);
            Boolean result = CallLogonToSAPWebServices.LogonToSAP(SAPTenantIDTxt.Text, UsernameTxt.Text, PasswordTxt.Text);
            Cursor.Current = Cursors.Default;

            if (result == true)
            {
                Globals.ThisAddIn.StoreUserCredentialsInRegistry();
                Globals.ThisAddIn.IsLoggedIntoSAP = true;
                
                MessageBox.Show(AppConstants.Logged_In_SAP, "Logon sucessfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Globals.ThisAddIn.UserLoginCustomControlPane.Visible = false;
            }
            else
            {
                Globals.ThisAddIn.IsLoggedIntoSAP = false;
                MessageBox.Show(AppConstants.Invalid_SAP_Credentials_Entered, "Logon Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SAPLogonControl_Load(object sender, EventArgs e)
        {
            if (Globals.ThisAddIn != null)
            {
                SAPTenantIDTxt.Text = Globals.ThisAddIn.TanentID;
                UsernameTxt.Text = Globals.ThisAddIn.Username;
                PasswordTxt.Text = Globals.ThisAddIn.Password;
            }
        }
    }
}
