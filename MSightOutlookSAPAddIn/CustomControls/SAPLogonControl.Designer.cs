﻿namespace MSightOutlookSAPAddIn.CustomControls
{
    partial class SAPLogonControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeaderLabel = new System.Windows.Forms.Label();
            this.UsernameLabel = new System.Windows.Forms.Label();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.UsernameTxt = new System.Windows.Forms.TextBox();
            this.PasswordTxt = new System.Windows.Forms.TextBox();
            this.SAPTenantIDTxt = new System.Windows.Forms.TextBox();
            this.SAPTenantIDLabel = new System.Windows.Forms.Label();
            this.LogonBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.AutoSize = true;
            this.HeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeaderLabel.Location = new System.Drawing.Point(4, 5);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(102, 16);
            this.HeaderLabel.TabIndex = 0;
            this.HeaderLabel.Text = "Logon to SAP";
            // 
            // UsernameLabel
            // 
            this.UsernameLabel.AutoSize = true;
            this.UsernameLabel.Location = new System.Drawing.Point(4, 91);
            this.UsernameLabel.Name = "UsernameLabel";
            this.UsernameLabel.Size = new System.Drawing.Size(55, 13);
            this.UsernameLabel.TabIndex = 1;
            this.UsernameLabel.Text = "Username";
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.Location = new System.Drawing.Point(4, 142);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(53, 13);
            this.PasswordLabel.TabIndex = 2;
            this.PasswordLabel.Text = "Password";
            // 
            // UsernameTxt
            // 
            this.UsernameTxt.Location = new System.Drawing.Point(7, 109);
            this.UsernameTxt.Name = "UsernameTxt";
            this.UsernameTxt.Size = new System.Drawing.Size(157, 20);
            this.UsernameTxt.TabIndex = 3;
            // 
            // PasswordTxt
            // 
            this.PasswordTxt.Location = new System.Drawing.Point(7, 161);
            this.PasswordTxt.Name = "PasswordTxt";
            this.PasswordTxt.PasswordChar = '*';
            this.PasswordTxt.Size = new System.Drawing.Size(157, 20);
            this.PasswordTxt.TabIndex = 4;
            // 
            // SAPTenantIDTxt
            // 
            this.SAPTenantIDTxt.Location = new System.Drawing.Point(7, 60);
            this.SAPTenantIDTxt.Name = "SAPTenantIDTxt";
            this.SAPTenantIDTxt.Size = new System.Drawing.Size(157, 20);
            this.SAPTenantIDTxt.TabIndex = 7;
            // 
            // SAPTenantIDLabel
            // 
            this.SAPTenantIDLabel.AutoSize = true;
            this.SAPTenantIDLabel.Location = new System.Drawing.Point(4, 40);
            this.SAPTenantIDLabel.Name = "SAPTenantIDLabel";
            this.SAPTenantIDLabel.Size = new System.Drawing.Size(65, 13);
            this.SAPTenantIDLabel.TabIndex = 6;
            this.SAPTenantIDLabel.Text = "SAP Tenant";
            // 
            // LogonBtn
            // 
            this.LogonBtn.Image = global::MSightOutlookSAPAddIn.Properties.Resources.SAP_Logon;
            this.LogonBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LogonBtn.Location = new System.Drawing.Point(34, 187);
            this.LogonBtn.Name = "LogonBtn";
            this.LogonBtn.Size = new System.Drawing.Size(103, 50);
            this.LogonBtn.TabIndex = 5;
            this.LogonBtn.Text = "Logon";
            this.LogonBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LogonBtn.UseVisualStyleBackColor = true;
            this.LogonBtn.Click += new System.EventHandler(this.LogonBtn_Click);
            // 
            // SAPLogonControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SAPTenantIDTxt);
            this.Controls.Add(this.SAPTenantIDLabel);
            this.Controls.Add(this.LogonBtn);
            this.Controls.Add(this.PasswordTxt);
            this.Controls.Add(this.UsernameTxt);
            this.Controls.Add(this.PasswordLabel);
            this.Controls.Add(this.UsernameLabel);
            this.Controls.Add(this.HeaderLabel);
            this.Name = "SAPLogonControl";
            this.Size = new System.Drawing.Size(197, 249);
            this.Load += new System.EventHandler(this.SAPLogonControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeaderLabel;
        private System.Windows.Forms.Label UsernameLabel;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.TextBox UsernameTxt;
        private System.Windows.Forms.TextBox PasswordTxt;
        private System.Windows.Forms.Button LogonBtn;
        private System.Windows.Forms.TextBox SAPTenantIDTxt;
        private System.Windows.Forms.Label SAPTenantIDLabel;
    }
}
