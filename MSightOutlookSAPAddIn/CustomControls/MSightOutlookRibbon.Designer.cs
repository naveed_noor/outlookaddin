﻿namespace MSightOutlookSAPAddIn.CustomControls
{
    partial class MSightOutlookRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public MSightOutlookRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.IsUserloggedInCheckBox = this.Factory.CreateRibbonCheckBox();
            this.group3 = this.Factory.CreateRibbonGroup();
            this.separator3 = this.Factory.CreateRibbonSeparator();
            this.label3 = this.Factory.CreateRibbonLabel();
            this.NameLbl = this.Factory.CreateRibbonLabel();
            this.InternalIDLbl = this.Factory.CreateRibbonLabel();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.separator1 = this.Factory.CreateRibbonSeparator();
            this.label1 = this.Factory.CreateRibbonLabel();
            this.LastSyncedDateTime = this.Factory.CreateRibbonLabel();
            this.LogonButton = this.Factory.CreateRibbonButton();
            this.SettingsBtn = this.Factory.CreateRibbonButton();
            this.SAPEmployeeLinkBtn = this.Factory.CreateRibbonButton();
            this.SyncAppointmentsButton = this.Factory.CreateRibbonButton();
            this.IsSyncingWithSAPChkbox = this.Factory.CreateRibbonCheckBox();
            this.tab1.SuspendLayout();
            this.group1.SuspendLayout();
            this.group3.SuspendLayout();
            this.group2.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.group1);
            this.tab1.Groups.Add(this.group3);
            this.tab1.Groups.Add(this.group2);
            this.tab1.Label = "Mind Sight";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Items.Add(this.LogonButton);
            this.group1.Items.Add(this.IsUserloggedInCheckBox);
            this.group1.Label = "SAP Logon Status";
            this.group1.Name = "group1";
            // 
            // IsUserloggedInCheckBox
            // 
            this.IsUserloggedInCheckBox.Enabled = false;
            this.IsUserloggedInCheckBox.Label = "Is User logged In";
            this.IsUserloggedInCheckBox.Name = "IsUserloggedInCheckBox";
            // 
            // group3
            // 
            this.group3.Items.Add(this.SAPEmployeeLinkBtn);
            this.group3.Items.Add(this.separator3);
            this.group3.Items.Add(this.label3);
            this.group3.Items.Add(this.NameLbl);
            this.group3.Items.Add(this.InternalIDLbl);
            this.group3.Label = "SAP Linked Employee Status";
            this.group3.Name = "group3";
            // 
            // separator3
            // 
            this.separator3.Name = "separator3";
            // 
            // label3
            // 
            this.label3.Label = "Linked Employee Info:";
            this.label3.Name = "label3";
            // 
            // NameLbl
            // 
            this.NameLbl.Label = "Name";
            this.NameLbl.Name = "NameLbl";
            // 
            // InternalIDLbl
            // 
            this.InternalIDLbl.Label = "SAP ID";
            this.InternalIDLbl.Name = "InternalIDLbl";
            // 
            // group2
            // 
            this.group2.Items.Add(this.SyncAppointmentsButton);
            this.group2.Items.Add(this.IsSyncingWithSAPChkbox);
            this.group2.Items.Add(this.SettingsBtn);
            this.group2.Items.Add(this.separator1);
            this.group2.Items.Add(this.label1);
            this.group2.Items.Add(this.LastSyncedDateTime);
            this.group2.Label = "SAP Appointments Sync. Status";
            this.group2.Name = "group2";
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            // 
            // label1
            // 
            this.label1.Label = "Last Sync Date Time";
            this.label1.Name = "label1";
            // 
            // LastSyncedDateTime
            // 
            this.LastSyncedDateTime.Label = "                  ";
            this.LastSyncedDateTime.Name = "LastSyncedDateTime";
            // 
            // LogonButton
            // 
            this.LogonButton.Image = global::MSightOutlookSAPAddIn.Properties.Resources.SAP_Logon;
            this.LogonButton.Label = "Logon";
            this.LogonButton.Name = "LogonButton";
            this.LogonButton.ShowImage = true;
            this.LogonButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.LogonButton_Click);
            // 
            // SettingsBtn
            // 
            this.SettingsBtn.Image = global::MSightOutlookSAPAddIn.Properties.Resources.Settings;
            this.SettingsBtn.Label = "Settings";
            this.SettingsBtn.Name = "SettingsBtn";
            this.SettingsBtn.ShowImage = true;
            this.SettingsBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SettingsBtn_Click);
            // 
            // SAPEmployeeLinkBtn
            // 
            this.SAPEmployeeLinkBtn.Image = global::MSightOutlookSAPAddIn.Properties.Resources.employee_image;
            this.SAPEmployeeLinkBtn.Label = "Link SAP Employee";
            this.SAPEmployeeLinkBtn.Name = "SAPEmployeeLinkBtn";
            this.SAPEmployeeLinkBtn.ShowImage = true;
            this.SAPEmployeeLinkBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SAPEmployeeLinkBtn_Click);
            // 
            // SyncAppointmentsButton
            // 
            this.SyncAppointmentsButton.Image = global::MSightOutlookSAPAddIn.Properties.Resources.Refresh;
            this.SyncAppointmentsButton.Label = "Sync Appointments";
            this.SyncAppointmentsButton.Name = "SyncAppointmentsButton";
            this.SyncAppointmentsButton.ShowImage = true;
            this.SyncAppointmentsButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SyncAppointmentsButton_Click);
            // 
            // IsSyncingWithSAPChkbox
            // 
            this.IsSyncingWithSAPChkbox.Enabled = false;
            this.IsSyncingWithSAPChkbox.Label = "Is Syncing with SAP";
            this.IsSyncingWithSAPChkbox.Name = "IsSyncingWithSAPChkbox";
            // 
            // MSightOutlookRibbon
            // 
            this.Name = "MSightOutlookRibbon";
            this.RibbonType = "Microsoft.Outlook.Appointment, Microsoft.Outlook.Explorer, Microsoft.Outlook.Task" +
    "";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.MSightOutlookRibbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.group3.ResumeLayout(false);
            this.group3.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton LogonButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SyncAppointmentsButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label1;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel LastSyncedDateTime;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SAPEmployeeLinkBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label3;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator3;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel NameLbl;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel InternalIDLbl;
        internal Microsoft.Office.Tools.Ribbon.RibbonCheckBox IsUserloggedInCheckBox;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group3;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SettingsBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonCheckBox IsSyncingWithSAPChkbox;
    }

   
}
