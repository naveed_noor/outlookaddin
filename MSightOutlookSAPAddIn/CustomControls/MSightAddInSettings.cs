﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MSightOutlookSAPAddIn.UtilClasses;

namespace MSightOutlookSAPAddIn.CustomControls
{
    public partial class MSightAddInSettings : UserControl
    {
        public MSightAddInSettings()
        {
            InitializeComponent();
            
           
        }
        private void MSightAddInSettings_Load(object sender, EventArgs e)
        {
            if (MSightOutlookSAPAddIn.Globals.ThisAddIn != null)
            if (MSightOutlookSAPAddIn.Globals.ThisAddIn.IsLoggedIntoSAP == true && MSightOutlookSAPAddIn.Globals.ThisAddIn.IsEmployeeResponsibleLinked == true)
            {
                SettingsStruct _SettingsStruct = MSightRegistryManager.LoadSettingsFromRegistry();
                this.EnableAppointmentSyncChkbox.Checked = _SettingsStruct.EnableStatus;
                this.SAPSyncIntervalMinutesTxt.Value = _SettingsStruct.InternvalInMinutes;
                this.SyncWithSAPDaysNumericUpdown.Value = _SettingsStruct.SyncWithSAPDays;
                this.AppointmentWorkingDaysNumericUpDown.Value = _SettingsStruct.AppointmentWorkingDays;
            }
        }

        private void EnableAppointmentSyncChkbox_CheckedChanged(object sender, EventArgs e)
        {
            this.SAPSyncIntervalMinutesTxt.Enabled = this.EnableAppointmentSyncChkbox.Checked;
        }

        private void SaveSettingsBtn_Click(object sender, EventArgs e)
        {
            if (MSightOutlookSAPAddIn.Globals.ThisAddIn.IsLoggedIntoSAP == false || MSightOutlookSAPAddIn.Globals.ThisAddIn.IsEmployeeResponsibleLinked == false)
            {
                MessageBox.Show(AppConstants.NotLoggedInAndNoEmployeeLinked,"Error",MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }

            if (this.SAPSyncIntervalMinutesTxt.Value < 15)
            {
                MessageBox.Show("Sync interval must be 15 minutes or more","Error", MessageBoxButtons.OK,MessageBoxIcon.Error,MessageBoxDefaultButton.Button1);
                return;
            }
            Cursor.Current = Cursors.WaitCursor;

            MSightRegistryManager.SaveSettingsFromRegistry(new SettingsStruct() { 
                                                                                    EnableStatus = this.EnableAppointmentSyncChkbox.Checked, 
                                                                                    InternvalInMinutes = this.SAPSyncIntervalMinutesTxt.Value, 
                                                                                    SyncWithSAPDays = this.SyncWithSAPDaysNumericUpdown.Value, 
                                                                                    AppointmentWorkingDays = this.AppointmentWorkingDaysNumericUpDown.Value 
                                                                                });
            MSightOutlookSAPAddIn.Globals.ThisAddIn.StartSyncProcess();
            Cursor.Current = Cursors.Default;
        }

      

        
    }
}
