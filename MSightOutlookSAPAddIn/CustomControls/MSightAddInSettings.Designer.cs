﻿namespace MSightOutlookSAPAddIn.CustomControls
{
    partial class MSightAddInSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SaveSettingsBtn = new System.Windows.Forms.Button();
            this.EnableAppointmentSyncChkbox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SAPSyncIntervalMinutesTxt = new System.Windows.Forms.NumericUpDown();
            this.SyncWithSAPDaysNumericUpdown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.AppointmentWorkingDaysNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.SAPSyncIntervalMinutesTxt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SyncWithSAPDaysNumericUpdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppointmentWorkingDaysNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(301, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mind Sight SAP Outlook AddIn Settings";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SaveSettingsBtn
            // 
            this.SaveSettingsBtn.Image = global::MSightOutlookSAPAddIn.Properties.Resources.save_sap;
            this.SaveSettingsBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SaveSettingsBtn.Location = new System.Drawing.Point(61, 211);
            this.SaveSettingsBtn.Name = "SaveSettingsBtn";
            this.SaveSettingsBtn.Size = new System.Drawing.Size(178, 50);
            this.SaveSettingsBtn.TabIndex = 1;
            this.SaveSettingsBtn.Text = "Save Settings";
            this.SaveSettingsBtn.UseVisualStyleBackColor = true;
            this.SaveSettingsBtn.Click += new System.EventHandler(this.SaveSettingsBtn_Click);
            // 
            // EnableAppointmentSyncChkbox
            // 
            this.EnableAppointmentSyncChkbox.AutoSize = true;
            this.EnableAppointmentSyncChkbox.Checked = true;
            this.EnableAppointmentSyncChkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnableAppointmentSyncChkbox.Location = new System.Drawing.Point(6, 63);
            this.EnableAppointmentSyncChkbox.Name = "EnableAppointmentSyncChkbox";
            this.EnableAppointmentSyncChkbox.Size = new System.Drawing.Size(272, 17);
            this.EnableAppointmentSyncChkbox.TabIndex = 2;
            this.EnableAppointmentSyncChkbox.Text = "Enable Background Sync of Appointments with SAP";
            this.EnableAppointmentSyncChkbox.UseVisualStyleBackColor = true;
            this.EnableAppointmentSyncChkbox.CheckedChanged += new System.EventHandler(this.EnableAppointmentSyncChkbox_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "SAP Sync Interval (min):";
            // 
            // SAPSyncIntervalMinutesTxt
            // 
            this.SAPSyncIntervalMinutesTxt.Location = new System.Drawing.Point(208, 95);
            this.SAPSyncIntervalMinutesTxt.Name = "SAPSyncIntervalMinutesTxt";
            this.SAPSyncIntervalMinutesTxt.Size = new System.Drawing.Size(98, 20);
            this.SAPSyncIntervalMinutesTxt.TabIndex = 4;
            this.SAPSyncIntervalMinutesTxt.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // SyncWithSAPDaysNumericUpdown
            // 
            this.SyncWithSAPDaysNumericUpdown.Location = new System.Drawing.Point(208, 128);
            this.SyncWithSAPDaysNumericUpdown.Name = "SyncWithSAPDaysNumericUpdown";
            this.SyncWithSAPDaysNumericUpdown.Size = new System.Drawing.Size(98, 20);
            this.SyncWithSAPDaysNumericUpdown.TabIndex = 6;
            this.SyncWithSAPDaysNumericUpdown.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "SAP Sync Interval (days) :";
            // 
            // AppointmentWorkingDaysNumericUpDown
            // 
            this.AppointmentWorkingDaysNumericUpDown.Location = new System.Drawing.Point(208, 182);
            this.AppointmentWorkingDaysNumericUpDown.Name = "AppointmentWorkingDaysNumericUpDown";
            this.AppointmentWorkingDaysNumericUpDown.Size = new System.Drawing.Size(98, 20);
            this.AppointmentWorkingDaysNumericUpDown.TabIndex = 8;
            this.AppointmentWorkingDaysNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(241, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Create Outlook Appointment (after working days) :";
            // 
            // MSightAddInSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.AppointmentWorkingDaysNumericUpDown);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SyncWithSAPDaysNumericUpdown);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.SAPSyncIntervalMinutesTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.EnableAppointmentSyncChkbox);
            this.Controls.Add(this.SaveSettingsBtn);
            this.Controls.Add(this.label1);
            this.Name = "MSightAddInSettings";
            this.Size = new System.Drawing.Size(316, 276);
            this.Load += new System.EventHandler(this.MSightAddInSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SAPSyncIntervalMinutesTxt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SyncWithSAPDaysNumericUpdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppointmentWorkingDaysNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SaveSettingsBtn;
        private System.Windows.Forms.CheckBox EnableAppointmentSyncChkbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown SAPSyncIntervalMinutesTxt;
        private System.Windows.Forms.NumericUpDown SyncWithSAPDaysNumericUpdown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown AppointmentWorkingDaysNumericUpDown;
        private System.Windows.Forms.Label label4;
    }
}
