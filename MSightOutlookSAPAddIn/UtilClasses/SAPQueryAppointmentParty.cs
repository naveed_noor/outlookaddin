using System;
using System.Collections.Generic;
using System.Linq;

namespace MSightOutlookSAPAddIn.UtilClasses
{
    public struct SAPQueryAppointmentParty
    {
        public PartyType PartyType;
        public String PartyID;
        public String PartyUUID;
        public String PartyTypeCode;
        public String PartyTypeName;

        public ContactInfo PartyContactInfo;
    }
}
