﻿using System;
using System.Collections.Generic;
using System.Linq;
using System ;
using System.Threading.Tasks;

namespace MSightOutlookSAPAddIn.UtilClasses
{
    public class MaintainLinkedEmployeeCredentials
    {

        public static Boolean ReadEmployeeCredentialsFromRegistry(out String InternalID, out String FirstName, out String LastName, out String Email)
        {
            MaintainRegisteryValues reg = new MaintainRegisteryValues();
            reg.ShowError = true;

            InternalID  = reg.Read(MaintainRegisteryValues.InternalID);
            FirstName  = reg.Read(MaintainRegisteryValues.FirstName);
            LastName  = reg.Read(MaintainRegisteryValues.LastName);
            Email  = reg.Read(MaintainRegisteryValues.Email);
            if (!String.IsNullOrEmpty(InternalID) && !String.IsNullOrEmpty(FirstName))
                return true;
            else 
                return false;
        }

        public static void StoreEmployeeCredentialsInRegistry(String InternalID, String FirstName, String LastName, String Email)
        {
            MaintainRegisteryValues reg = new MaintainRegisteryValues();
            reg.ShowError = true;
            reg.Write(MaintainRegisteryValues.InternalID, InternalID );
            reg.Write(MaintainRegisteryValues.FirstName, FirstName );
            reg.Write(MaintainRegisteryValues.LastName, LastName );
            reg.Write(MaintainRegisteryValues.Email, Email );
        }

    }
}
