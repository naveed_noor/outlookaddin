using System;
using System.Collections.Generic;
using System.Linq;

namespace MSightOutlookSAPAddIn.UtilClasses
{
    public struct SAPQueryAppointment
    {
        public String ID;
        public String UUID;
        public String Name;
        public String PriorityName;
        public String GroupName;
        public String LifeCycleStatusName;
        public String LastChangeIdentityID;
        public String LastChangeIdentityName;
        public String CreationIdentityID;
        public String CreationIdentityName;
        public String Notes;
        public DateTime ScheduledPeriod_StartDateTime;
        public DateTime ScheduledPeriod_EndDateTime;

        public String AppointmentID;
        public String MeetingPlace;

        public List<SAPQueryAppointmentParty> SAPQueryAppointmentPartyList;

        public bool FullDayIndicator;
    }
}