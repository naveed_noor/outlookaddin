using MSightOutlookSAPAddIn.UtilClasses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MSightOutlookSAPAddIn.UtilClasses
{
    public struct AppointmentStruct
    {
        public String AppointmentOID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Boolean IsAllDayEvent { get; set; }
        public String Location { get; set; }
        public String Body { get; set; }
        public String Subject { get; set; }
        public List<ContactInfo> Receipients { get; set; }

    }
}
