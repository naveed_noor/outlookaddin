using System;
using System.Collections.Generic;
using System.Linq;

namespace MSightOutlookSAPAddIn.UtilClasses
{
    public struct ContactInfo
    {
        public String FirstName;
        public String LastName;
        public String EmailAddress;
        public String InternalID;
        
        public String PhoneNo;
        public String CountryCode;
        public String RegionCode;
        public String CityName;
        public String StreetPostalCode;
        public String StreetPrefixName;

        public RecipientType RecipientType;
    }


    public enum RecipientType
    {
        // Summary:
        //     Meeting organizer
        Organizer = 0,
        //
        // Summary:
        //     Required attendee
        Required = 1,
        //
        // Summary:
        //     Optional attendee
        Optional = 2,
        //
        // Summary:
        //     A resource such as a conference room
        Resource = 3
    }

    public enum BusinessParnerCategoryCode
    {
        NotKnown = 0,
        Employee = 1,
        BusniessParnner = 2
    }

    public enum PartyType
    {
        Organizer = 0,
        EmployeeResponsible = 1,
        Account = 2,
        Attendee = 3
    }
}
