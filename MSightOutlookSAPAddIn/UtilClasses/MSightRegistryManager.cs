﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSightOutlookSAPAddIn.UtilClasses
{
    public class MSightRegistryManager
    {
        public static SettingsStruct LoadSettingsFromRegistry()
        {
            SettingsStruct _SettingsStruct = new SettingsStruct();

            Boolean isDataFoundInRegistry = true;
            MaintainRegisteryValues reg = new MaintainRegisteryValues();
            reg.ShowError = true;
            string EnableSyncInBackgroundRegVal = reg.Read(MaintainRegisteryValues.EnableSyncInBackground);
            if (EnableSyncInBackgroundRegVal != null)
                _SettingsStruct.EnableStatus = Boolean.Parse(EnableSyncInBackgroundRegVal.ToString());
            else
            {
                _SettingsStruct.EnableStatus = true;
                isDataFoundInRegistry = false;
            }

            string SyncIntervalInMinutesregVal = reg.Read(MaintainRegisteryValues.SyncIntervalInMinutes);
            if (SyncIntervalInMinutesregVal != null)
                _SettingsStruct.InternvalInMinutes = decimal.Parse(SyncIntervalInMinutesregVal.ToString());
            else
            {
                _SettingsStruct.InternvalInMinutes = 15;
                isDataFoundInRegistry = false;
            }

            string SyncWithSAPDaysVal = reg.Read(MaintainRegisteryValues.SyncWithSAPDays);
            if (SyncWithSAPDaysVal != null)
                _SettingsStruct.SyncWithSAPDays = decimal.Parse(SyncWithSAPDaysVal.ToString());
            else
            {
                _SettingsStruct.SyncWithSAPDays = 3;
                isDataFoundInRegistry = false;
            }

            string AppointmentWorkingDaysVal = reg.Read(MaintainRegisteryValues.AppointmentWorkingDays);
            if (AppointmentWorkingDaysVal != null)
                _SettingsStruct.AppointmentWorkingDays = decimal.Parse(AppointmentWorkingDaysVal.ToString());
            else
            {
                _SettingsStruct.AppointmentWorkingDays = 10;
                isDataFoundInRegistry = false;
            }

            if(isDataFoundInRegistry == false)
            {
                MSightRegistryManager.SaveSettingsFromRegistry(new SettingsStruct() { EnableStatus = true, InternvalInMinutes = 15, SyncWithSAPDays = 3, AppointmentWorkingDays = 10 });
            }
            return _SettingsStruct;
        }

        public static void SaveSettingsFromRegistry(SettingsStruct _SettingsStruct)
        {
            MaintainRegisteryValues reg = new MaintainRegisteryValues();
            reg.ShowError = true;
            reg.Write(MaintainRegisteryValues.EnableSyncInBackground, _SettingsStruct.EnableStatus.ToString());
            reg.Write(MaintainRegisteryValues.SyncIntervalInMinutes, _SettingsStruct.InternvalInMinutes.ToString());
            reg.Write(MaintainRegisteryValues.SyncWithSAPDays, _SettingsStruct.SyncWithSAPDays.ToString());
            reg.Write(MaintainRegisteryValues.AppointmentWorkingDays, _SettingsStruct.AppointmentWorkingDays.ToString());
        }
    }


    public struct SettingsStruct { 
        public bool EnableStatus; 
        public decimal InternvalInMinutes;
        public decimal SyncWithSAPDays;
        public decimal AppointmentWorkingDays;
    }
}
