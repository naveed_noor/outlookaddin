﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSightOutlookSAPAddIn.UtilClasses
{
    public class AppConstants
    {

        public static readonly String InternalSAPUserName = "_MIDSIGHT";
        public static readonly String InternalSAPPassword = "MSight_IPP1";

        public static readonly String TestTenantID = "307263";
        public static readonly String PRDTenantID = "307263";

        public static readonly String NotLoggedInAndNoEmployeeLinked = "Please login to SAP and set the Linked Employee to Start Syncing Appointments";

        public static readonly String Enter_Project_ID = "Enter project ID to search from SAP";
        public static readonly String Enter_Exact_Project_ID = "Enter Exact Project ID to search from SAP";
        public static readonly String All_Projects_Loading_Not_Allowed = "You can't Enter * to load all project data in this screen";
        public static readonly String No_Project_Found_In_SAP = "No Project found in SAP against the entered Project ID";
        public static readonly String More_Then_One_Project_Returned = "This Project ID returned more then Projects from SAP. Please enter exact Project ID which returns only one Project from SAP";
        public static readonly String Multiple_Project_Message_Title = "Multiple Projects Returned from SAP";
        public static readonly String First_Load_Project = "Please First Load Project from SAP to Create Baseline";
        public static readonly String Already_Logged_In_SAP = "Already Logged in SAP";
        public static readonly String Already_Logged_In_SAP_Title = "Logged in SAP";
        public static readonly String Enter_SAP_Tenant_ID = "Please Enter SAP Tanent ID";
        public static readonly String Enter_User_Name = "Please Enter SAP Username";
        public static readonly String Enter_Password = "Please Enter SAP Password";
        public static readonly String Logged_In_SAP = "Sucessfully Logged into SAP";
        public static readonly String Invalid_SAP_Credentials_Entered = "Invalid credentials entered, Please correct and then try again to logon to SAP.";

        public static readonly String Load_All_Project_Confirmation = "Are you sure to load all project from SAP, this may take long itme";
        public static readonly String Refresh_Lookups_From_SAP = "Are you sure to refresh lookup lists from SAP";
        public static readonly String Lookup_Lists_Loadded = "Lookup lists data refreshed from SAP";



        public static readonly String Project_BaseLine_Created = "Project Baseline Created in SAP, However SAP also retuned following message";
        public static readonly String Project_BaseLine_Not_Created = "Project Baseline Not Created in SAP, and SAP retuned following Error message";
    }
}
