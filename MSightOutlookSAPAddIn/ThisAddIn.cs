﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools;
using Microsoft.Office.Interop.Outlook;
using MSightOutlookSAPAddIn.SAPWebServices;
using System.Security.Cryptography;
using System.IO;
using MSightOutlookSAPAddIn.UtilClasses;
using MSightOutlookSAPAddIn.CustomControls;
using System.ComponentModel;
using System.Windows.Forms;
using MSightOutlookSAPAddIn.MSightManagers;

namespace MSightOutlookSAPAddIn
{
    public partial class ThisAddIn
    {

        BackgroundWorker myWorker;
        public bool _isRunning;
        Timer t = new Timer();

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            LoadUserSettingsFromRegistry();
            StartSyncProcess();
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }


        #region Appointment Loading and refresh related Functions

        public void StartSyncProcess()
        {
            myWorker = new BackgroundWorker();
            var ribbon = (MSightOutlookRibbon)Globals.Ribbons.GetRibbon(typeof(MSightOutlookRibbon));

            
            SettingsStruct _SettingsStruct = MSightRegistryManager.LoadSettingsFromRegistry();
            if (_SettingsStruct.EnableStatus == true && this._IsLoggedIntoSAP == true && this.IsEmployeeResponsibleLinked == true)
            {
               
                myWorker.DoWork += myWorker_DoWork;

                t.Interval = int.Parse(_SettingsStruct.InternvalInMinutes.ToString()) * 60000; // minutes x milliseconds
                t.Tick += t_Tick;
                t.Enabled = true;
                t.Start();
                if (ribbon != null)
                    ribbon.IsSyncingWithSAPChkbox.Checked = true;
            }

            if (_SettingsStruct.EnableStatus == false)
            {
                myWorker.DoWork -= myWorker_DoWork;
                t.Enabled = false;
                t.Stop();
                if (ribbon != null)
                    ribbon.IsSyncingWithSAPChkbox.Checked = false;
            }
        }

        void t_Tick(object sender, EventArgs e)
        {
            if (_isRunning == false)
                myWorker.RunWorkerAsync();
        }
     
        void myWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                _isRunning = true;
                MSightAppointmentsManager.RefreshAppointments();
                MessageBox.Show("Appointments Synced with SAP", "Synced", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _isRunning = false;
            }
            catch (System.Exception exp)
            {
                _isRunning = false;
                MessageBox.Show("Some Error Occured while syncing with SAP, Please Consult with Mind Sight Admin for support.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show("Error Message is : " + exp.Message + " and further details are : " + exp.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion


        #region User Credentials and Linked Employee Management Functions

        public void LoadUserSettingsFromRegistry()
        {
            ReadUserCredentialsFromRegistry();

            String InternalID, FirstName, LastName, Email;
            Boolean res = MaintainLinkedEmployeeCredentials.ReadEmployeeCredentialsFromRegistry(out InternalID, out FirstName, out LastName, out Email);
            if (res == true)
            {
                this.InternalID = InternalID;
                this.FirstName = FirstName;
                this.LastName = LastName;
                this.Email = Email;
                this.IsEmployeeResponsibleLinked = true;
            }
            else
                this.IsEmployeeResponsibleLinked = false;
        }

        public CustomTaskPane UserLoginCustomControlPane;
        public CustomTaskPane SAPEmployeeLinkControl;
        public CustomTaskPane MSightSettingsTaskPane;

        private Boolean _IsLoggedIntoSAP;
        public Boolean IsLoggedIntoSAP 
        { 
            get { return _IsLoggedIntoSAP; } 
            set 
            { 
                _IsLoggedIntoSAP = value;
                var ribbon = (MSightOutlookRibbon)Globals.Ribbons.GetRibbon(typeof(MSightOutlookRibbon));
                if (value == true)
                {
                    if (ribbon != null)
                        ribbon.IsUserloggedInCheckBox.Checked = true;
                }
                else if (value == false)
                {
                    if (ribbon != null)
                        ribbon.IsUserloggedInCheckBox.Checked = false;
                }
            }
        }
        public String TanentID { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }

        Boolean _IsEmployeeResponsibleLinked;
        public Boolean IsEmployeeResponsibleLinked 
        {
            get { return _IsEmployeeResponsibleLinked; }
            set 
            {
                _IsEmployeeResponsibleLinked = value;
                if (value == true)                
                {
                    var ribbon = (MSightOutlookRibbon)Globals.Ribbons.GetRibbon(typeof(MSightOutlookRibbon));
                    if (ribbon != null)
                    {
                        ribbon.NameLbl.Label = String.Format("Name: {0} {1}", this.FirstName, this.LastName);
                        ribbon.InternalIDLbl.Label = String.Format("Internal ID: {0}", this.InternalID);
                    }
                }
            } 
        }

        public String InternalID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Email { get; set; }
        public void SetUserCredentials(String _TanentID, String _Username, String _Password)
        {
            Username = _Username;
            Password = _Password;
            TanentID = _TanentID;
        }

        private void ReadUserCredentialsFromRegistry()
        {
            MaintainRegisteryValues reg = new MaintainRegisteryValues();
            reg.ShowError = true;

            this.TanentID = reg.Read(MaintainRegisteryValues.SAPTanentID);
            this.Username = reg.Read(MaintainRegisteryValues.SAPUserName);
            this.Password = reg.Read(MaintainRegisteryValues.SAPUserPassword);
            if (this.Password != null)
                this.Password = MaintainUserCredentials.DecryptText(this.Password);

            if (this.Password != null && this.Username != null && this.TanentID != null)
            {
                Boolean result = CallLogonToSAPWebServices.LogonToSAP(this.TanentID, this.Username, this.Password);
                if (result == true)
                    IsLoggedIntoSAP = true;
                else
                    IsLoggedIntoSAP = false;
            }
        }

        public void StoreUserCredentialsInRegistry()
        {
            MaintainRegisteryValues reg = new MaintainRegisteryValues();
            reg.ShowError = true;
            reg.Write(MaintainRegisteryValues.SAPTanentID, TanentID);
            reg.Write(MaintainRegisteryValues.SAPUserName, Username);
            reg.Write(MaintainRegisteryValues.SAPUserPassword, MaintainUserCredentials.EncryptText(Password));
        }

        #endregion

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }

}