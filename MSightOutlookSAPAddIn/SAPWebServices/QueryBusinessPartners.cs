﻿using MSightOutlookSAPAddIn.UtilClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSightOutlookSAPAddIn.SAPWebServices
{

    public class QueryBusinessPartners
    {

        public static List<ContactInfo> GetBusinessPartnersInfoFromSAP(String BusinessParnerID, BusinessParnerCategoryCode BusinessParnerCategoryCode)
        {
            List<ContactInfo> _BusinessPartnersInfoList = new List<ContactInfo>();
            try
            {
                SAPQueryBusinessPartnerIn.BusinessPartnerResponseMessage_sync response = CallWebService(BusinessParnerID, BusinessParnerCategoryCode);

                if (response != null && response.BusinessPartner != null && response.BusinessPartner.Length > 0)
                    foreach (SAPQueryBusinessPartnerIn.BusinessPartnerResponseBusinessPartner item in response.BusinessPartner)
                    {
                        ContactInfo _BusinessPartnersInfoObj = new ContactInfo();
                        if (BusinessParnerCategoryCode == UtilClasses.BusinessParnerCategoryCode.Employee)
                        {
                            _BusinessPartnersInfoObj.InternalID = item.InternalID;
                            _BusinessPartnersInfoObj.FirstName = item.Person != null ? item.Person.GivenName : "";
                            _BusinessPartnersInfoObj.LastName = item.Person != null ? item.Person.FamilyName : "";
                            if (item.AddressInformation != null)
                                foreach (var AddItem in item.AddressInformation)
                                {
                                    if (AddItem.Address != null && AddItem.Address.EmailURI != null)
                                        _BusinessPartnersInfoObj.EmailAddress = AddItem.Address.EmailURI.Value;
                                }
                        }
                        else if (BusinessParnerCategoryCode == UtilClasses.BusinessParnerCategoryCode.BusniessParnner)
                        {
                            _BusinessPartnersInfoObj.InternalID = item.InternalID;
                            _BusinessPartnersInfoObj.FirstName = item.Organisation != null ? item.Organisation.FirstLineName : "";
                            _BusinessPartnersInfoObj.LastName = item.Organisation != null ? item.Organisation.SecondLineName : "";
                            if (item.AddressInformation != null)
                                foreach (var AddItem in item.AddressInformation)
                                {
                                    if (AddItem.Address != null)
                                    {
                                        if (AddItem.Address.EmailURI != null)
                                            _BusinessPartnersInfoObj.EmailAddress = AddItem.Address.EmailURI.Value;

                                        if (AddItem.Address.Telephone != null && AddItem.Address.Telephone.Length > 0)
                                            _BusinessPartnersInfoObj.PhoneNo = AddItem.Address.Telephone[0].FormattedNumberDescription;

                                        if (AddItem.Address.PostalAddress != null)
                                        {
                                            _BusinessPartnersInfoObj.CountryCode = AddItem.Address.PostalAddress.CountryCode;
                                            _BusinessPartnersInfoObj.RegionCode = AddItem.Address.PostalAddress.RegionCode.Value;
                                            _BusinessPartnersInfoObj.CityName = AddItem.Address.PostalAddress.CityName;
                                            _BusinessPartnersInfoObj.StreetPostalCode = AddItem.Address.PostalAddress.StreetPostalCode;
                                            _BusinessPartnersInfoObj.StreetPrefixName = AddItem.Address.PostalAddress.StreetPrefixName;
                                        }
                                    }
                                }
                        }

                        _BusinessPartnersInfoList.Add(_BusinessPartnersInfoObj);
                    }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("The following error occurred while Querying Business Partners Information from SAP: " + ex.Message);
            }
            return _BusinessPartnersInfoList;
        }


        private static SAPQueryBusinessPartnerIn.BusinessPartnerResponseMessage_sync CallWebService(String BusinessParnerID, BusinessParnerCategoryCode BusinessParnerCategoryCode)
        {
            //Create the Web Service reference service object
            SAPQueryBusinessPartnerIn.service svc = new SAPQueryBusinessPartnerIn.service();
            //Set Credentials

            String uName = Globals.ThisAddIn.Username;
            String uPassword = Globals.ThisAddIn.Password;

            Globals.ThisAddIn.Username = AppConstants.InternalSAPUserName;
            Globals.ThisAddIn.Password = AppConstants.InternalSAPPassword;
            svc.Credentials = new SAPCredentials();


            //Create Request object
            SAPQueryBusinessPartnerIn.BusinessPartnerByIdentificationQueryMessage_sync request = new SAPQueryBusinessPartnerIn.BusinessPartnerByIdentificationQueryMessage_sync();

            request.BusinessPartnerSelectionByIdentification = new SAPQueryBusinessPartnerIn.BusinessPartnerSelectionByIdentification();

            request.BusinessPartnerSelectionByIdentification.SelectionByInternalID = new SAPQueryBusinessPartnerIn.SelectionByIdentifier[1];

            request.BusinessPartnerSelectionByIdentification.SelectionByInternalID[0] = new SAPQueryBusinessPartnerIn.SelectionByIdentifier();
            request.BusinessPartnerSelectionByIdentification.SelectionByInternalID[0].InclusionExclusionCode = "I";
            request.BusinessPartnerSelectionByIdentification.SelectionByInternalID[0].IntervalBoundaryTypeCode = "1";
            request.BusinessPartnerSelectionByIdentification.SelectionByInternalID[0].LowerBoundaryIdentifier = BusinessParnerID;

            request.BusinessPartnerSelectionByIdentification.SelectionByCategoryCode = new SAPQueryBusinessPartnerIn.SelectionByCode[1];

            request.BusinessPartnerSelectionByIdentification.SelectionByCategoryCode[0] = new SAPQueryBusinessPartnerIn.SelectionByCode();
            request.BusinessPartnerSelectionByIdentification.SelectionByCategoryCode[0].InclusionExclusionCode = "I";
            request.BusinessPartnerSelectionByIdentification.SelectionByCategoryCode[0].IntervalBoundaryTypeCode = "1";
            request.BusinessPartnerSelectionByIdentification.SelectionByCategoryCode[0].LowerBoundaryCode = new SAPQueryBusinessPartnerIn.Code();

            if (BusinessParnerCategoryCode == UtilClasses.BusinessParnerCategoryCode.NotKnown)
                request.BusinessPartnerSelectionByIdentification.SelectionByCategoryCode[0].LowerBoundaryCode.Value = "*";
            else
                request.BusinessPartnerSelectionByIdentification.SelectionByCategoryCode[0].LowerBoundaryCode.Value = ((Int32)BusinessParnerCategoryCode).ToString();

            request.BusinessPartnerSelectionByIdentification.SelectionByLifeCycleStatusCode = new SAPQueryBusinessPartnerIn.SelectionByCode[1];

            request.BusinessPartnerSelectionByIdentification.SelectionByLifeCycleStatusCode[0] = new SAPQueryBusinessPartnerIn.SelectionByCode();
            request.BusinessPartnerSelectionByIdentification.SelectionByLifeCycleStatusCode[0].InclusionExclusionCode = "I";
            request.BusinessPartnerSelectionByIdentification.SelectionByLifeCycleStatusCode[0].IntervalBoundaryTypeCode = "1";
            request.BusinessPartnerSelectionByIdentification.SelectionByLifeCycleStatusCode[0].LowerBoundaryCode = new SAPQueryBusinessPartnerIn.Code();
            request.BusinessPartnerSelectionByIdentification.SelectionByLifeCycleStatusCode[0].LowerBoundaryCode.Value = "2";


            request.ProcessingConditions = new SAPQueryBusinessPartnerIn.QueryProcessingConditions();
            request.ProcessingConditions.QueryHitsMaximumNumberValue = 0;
            request.ProcessingConditions.QueryHitsUnlimitedIndicator = true;
            request.ProcessingConditions.QueryHitsMaximumNumberValueSpecified = true;

            try
            {
                //Make the call, passing the request
                SAPQueryBusinessPartnerIn.BusinessPartnerResponseMessage_sync response = svc.FindByIdentification(request);
                Globals.ThisAddIn.Username = uName;
                Globals.ThisAddIn.Password = uPassword;
                return response;
            }
            catch (Exception exp)
            {
                Globals.ThisAddIn.Username = uName;
                Globals.ThisAddIn.Password = uPassword;
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
            }
            return null;
        }


        public static ContactInfo GetBusinessPartnersInfoFromSAP(String FamilyName, String GivenName)
        {
            ContactInfo _BusinessPartnersInfoObj = new ContactInfo();
            try
            {
                SAPQueryBusinessPartnerIn.BusinessPartnerResponseMessage_sync response = CallWebService(FamilyName, GivenName);

                if (response != null && response.BusinessPartner != null && response.BusinessPartner.Length > 0)
                    foreach (SAPQueryBusinessPartnerIn.BusinessPartnerResponseBusinessPartner item in response.BusinessPartner)
                    {
                        _BusinessPartnersInfoObj.InternalID = item.InternalID;
                        _BusinessPartnersInfoObj.FirstName = item.Person != null ? item.Person.GivenName : "";
                        _BusinessPartnersInfoObj.LastName = item.Person != null ? item.Person.FamilyName : "";
                        if (item.AddressInformation != null)
                            foreach (var AddItem in item.AddressInformation)
                            {
                                if (AddItem.Address != null && AddItem.Address.EmailURI != null)
                                    _BusinessPartnersInfoObj.EmailAddress = AddItem.Address.EmailURI.Value;
                            }
                        return _BusinessPartnersInfoObj;
                    }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("The following error occurred while Querying Business Partners Information from SAP: " + ex.Message);
            }
            return _BusinessPartnersInfoObj;
        }


        private static SAPQueryBusinessPartnerIn.BusinessPartnerResponseMessage_sync CallWebService(String FamilyName, String GivenName)
        {
            //Create the Web Service reference service object
            SAPQueryBusinessPartnerIn.service svc = new SAPQueryBusinessPartnerIn.service();
            //Set Credentials

            String uName = Globals.ThisAddIn.Username;
            String uPassword = Globals.ThisAddIn.Password;

            Globals.ThisAddIn.Username = AppConstants.InternalSAPUserName;
            Globals.ThisAddIn.Password = AppConstants.InternalSAPPassword;
            svc.Credentials = new SAPCredentials();


            //Create Request object
            SAPQueryBusinessPartnerIn.BusinessPartnerByIdentificationQueryMessage_sync request = new SAPQueryBusinessPartnerIn.BusinessPartnerByIdentificationQueryMessage_sync();

            request.BusinessPartnerSelectionByIdentification = new SAPQueryBusinessPartnerIn.BusinessPartnerSelectionByIdentification();

            request.BusinessPartnerSelectionByIdentification.SelectionByFormattedName = new SAPQueryBusinessPartnerIn.SelectionByName[1];

            request.BusinessPartnerSelectionByIdentification.SelectionByFormattedName[0] = new SAPQueryBusinessPartnerIn.SelectionByName();
            request.BusinessPartnerSelectionByIdentification.SelectionByFormattedName[0].InclusionExclusionCode = "I";
            request.BusinessPartnerSelectionByIdentification.SelectionByFormattedName[0].IntervalBoundaryTypeCode = "1";
            request.BusinessPartnerSelectionByIdentification.SelectionByFormattedName[0].LowerBoundaryName = GivenName+ " " +FamilyName;

            request.ProcessingConditions = new SAPQueryBusinessPartnerIn.QueryProcessingConditions();
            request.ProcessingConditions.QueryHitsMaximumNumberValue = 0;
            request.ProcessingConditions.QueryHitsUnlimitedIndicator = true;
            request.ProcessingConditions.QueryHitsMaximumNumberValueSpecified = true;

            try
            {
                //Make the call, passing the request
                SAPQueryBusinessPartnerIn.BusinessPartnerResponseMessage_sync response = svc.FindByIdentification(request);
                Globals.ThisAddIn.Username = uName;
                Globals.ThisAddIn.Password = uPassword;
                return response;
            }
            catch (Exception exp)
            {
                Globals.ThisAddIn.Username = uName;
                Globals.ThisAddIn.Password = uPassword;
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
            }
            return null;
        }
    }
}
