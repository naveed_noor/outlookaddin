﻿using MSightOutlookSAPAddIn.UtilClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSightOutlookSAPAddIn.SAPWebServices
{
    public class SAPQueryEmployeeManager
    {

        public List<SAPEmployeeInfo> GetEmployeeInfoFromSAP(String EmployeeID)
        {
            List<SAPEmployeeInfo> SAPEmployeeInfoList = new List<SAPEmployeeInfo>();
            SAPQueryEmployeeIn.EmployeeDataSimpleByResponseMessage_sync response = CallWebService(EmployeeID);
            if (response != null && response.EmployeeData != null && response.EmployeeData.Length > 0)
                foreach (SAPQueryEmployeeIn.EmployeeDataResponseEmployee item in response.EmployeeData)
                 {
                     SAPEmployeeInfo _SAPEmployeeInfo = new SAPEmployeeInfo();
                     _SAPEmployeeInfo.EmployeeID = item.EmployeeID.Value;
                     _SAPEmployeeInfo.FamilyName = item.BiographicalData[0].FamilyName;
                     _SAPEmployeeInfo.FamilyName = item.BiographicalData[0].GivenName;
                     if (item.WorkplaceAddressInformation != null && item.AddressInformation.Length > 0 &&
                            item.AddressInformation[0].Address != null && item.AddressInformation[0].Address.Email.Length > 0 && 
                            item.AddressInformation[0].Address.Email[0].URI != null)
                    {
                        _SAPEmployeeInfo.EmailAddress = item.AddressInformation[0].Address.Email[0].URI.Value;
                    }

                     SAPEmployeeInfoList.Add(_SAPEmployeeInfo);
                 }
            return SAPEmployeeInfoList;
        }


        private static SAPQueryEmployeeIn.EmployeeDataSimpleByResponseMessage_sync CallWebService(String EmployeeID)
        {
            //Create the Web Service reference service object
            SAPQueryEmployeeIn.service svc = new SAPQueryEmployeeIn.service();
            //Set Credentials

            String uName = Globals.ThisAddIn.Username;
            String uPassword = Globals.ThisAddIn.Password;

            Globals.ThisAddIn.Username = AppConstants.InternalSAPUserName;
            Globals.ThisAddIn.Password = AppConstants.InternalSAPPassword;
            svc.Credentials = new SAPCredentials();


            //Create Request object
            SAPQueryEmployeeIn.EmployeeDataSimpleByQueryMessage_sync request = new SAPQueryEmployeeIn.EmployeeDataSimpleByQueryMessage_sync();

            request.EmployeeDataSelectionByIdentification = new SAPQueryEmployeeIn.EmployeeDataSelectionByIdentification();

            request.EmployeeDataSelectionByIdentification.SelectionByEmployeeID = new SAPQueryEmployeeIn.EmployeeSelectionByEmployeeID[1];

            request.EmployeeDataSelectionByIdentification.SelectionByEmployeeID[0] = new SAPQueryEmployeeIn.EmployeeSelectionByEmployeeID();
            request.EmployeeDataSelectionByIdentification.SelectionByEmployeeID[0].InclusionExclusionCode = "I";
            request.EmployeeDataSelectionByIdentification.SelectionByEmployeeID[0].IntervalBoundaryTypeCode = "1";
            request.EmployeeDataSelectionByIdentification.SelectionByEmployeeID[0].LowerBoundaryEmployeeID = new SAPQueryEmployeeIn.EmployeeID();
            request.EmployeeDataSelectionByIdentification.SelectionByEmployeeID[0].LowerBoundaryEmployeeID.Value = EmployeeID;

            request.PROCESSING_CONDITIONS = new SAPQueryEmployeeIn.QueryProcessingConditions();
            request.PROCESSING_CONDITIONS.QueryHitsMaximumNumberValue = 0;
            request.PROCESSING_CONDITIONS.QueryHitsUnlimitedIndicator = true;
            request.PROCESSING_CONDITIONS.QueryHitsMaximumNumberValueSpecified = true;

            try
            {
                //Make the call, passing the request
                SAPQueryEmployeeIn.EmployeeDataSimpleByResponseMessage_sync response = svc.FindByIdentification(request);
                Globals.ThisAddIn.Username = uName;
                Globals.ThisAddIn.Password = uPassword;
                return response;
            }
            catch (Exception exp)
            {
                Globals.ThisAddIn.Username = uName;
                Globals.ThisAddIn.Password = uPassword;
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
            }
            return null;
        }
    }

    public class SAPEmployeeInfo
    {
        public String EmployeeID;
        /// <summary>
        /// Lower boundry name (First Name)
        /// </summary>
        public String FamilyName; 
        /// <summary>
        /// Upper boundry name (Last Name)
        /// </summary>
        public String GivenName; //

        public String EmailAddress;
    }
}
