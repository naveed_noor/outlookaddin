﻿using MSightOutlookSAPAddIn.UtilClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSightOutlookSAPAddIn.SAPWebServices
{
    public class QueryCustomerQuoteInManager
    {
        public SAPCustomerQuote QueryCustomerQuoteInFromSAP(String SalesQuoteID)
        {
            SAPQueryCustomerQuoteIn.CustomerQuoteByElementsResponseMessage_sync response = CallWebService(SalesQuoteID);
            
            if(response != null && response.CustomerQuote != null && response.CustomerQuote.Length > 0)
            {
                SAPCustomerQuote _SAPCustomerQuoteObject = new SAPCustomerQuote();
                foreach (var item in response.CustomerQuote)
                {
                    _SAPCustomerQuoteObject.ID = item.ID.Value;
                    _SAPCustomerQuoteObject.PostingDate = item.PostingDate;

                    if (item.EmployeeResponsibleParty != null && item.EmployeeResponsibleParty.PartyID != null)
                        _SAPCustomerQuoteObject.EmployeeResponsiblePartyID = item.EmployeeResponsibleParty.PartyID.Value;

                    if (item.EmployeeResponsibleParty != null && item.EmployeeResponsibleParty.Address != null
                            && item.EmployeeResponsibleParty.Address.Email != null && item.EmployeeResponsibleParty.Address.Email.Length > 0 && item.EmployeeResponsibleParty.Address.Email[0].URI != null )
                        _SAPCustomerQuoteObject.EmployeeResponsiblePartyEmail = item.EmployeeResponsibleParty.Address.Email[0].URI.Value;
                    
                    _SAPCustomerQuoteObject.ChanceofSuccess = item.ChanceofSuccess;
                    _SAPCustomerQuoteObject.SalesPhase = item.ExtSalesPhase;
                    _SAPCustomerQuoteObject.Source = item.Source;
                    _SAPCustomerQuoteObject.SalesCycle = item.ExtSalesCycle;
                    _SAPCustomerQuoteObject.SalesRep = item.SalesRepID;
                    if(_SAPCustomerQuoteObject.SalesRep != null)
                    {
                        List<ContactInfo> _BusinessPartnersInfoList = QueryBusinessPartners.GetBusinessPartnersInfoFromSAP(_SAPCustomerQuoteObject.SalesRep,BusinessParnerCategoryCode.Employee);
                        if (_BusinessPartnersInfoList != null && _BusinessPartnersInfoList.Count > 0)
                        {
                            _SAPCustomerQuoteObject.SalesRep_EmailAddress = _BusinessPartnersInfoList[0].EmailAddress;
                            _SAPCustomerQuoteObject.SalesRep_FamilyName = _BusinessPartnersInfoList[0].FirstName;
                            _SAPCustomerQuoteObject.SalesRep_GivenName = _BusinessPartnersInfoList[0].LastName;
                        }
                        else
                        {
                            SAPQueryEmployeeManager _SAPQueryEmployeeManager = new SAPQueryEmployeeManager();
                            List<SAPEmployeeInfo> SAPEmployeeInfoList = _SAPQueryEmployeeManager.GetEmployeeInfoFromSAP(_SAPCustomerQuoteObject.SalesRep);
                            if (SAPEmployeeInfoList.Count > 0)
                            {
                                SAPEmployeeInfo _SAPEmployeeInfoObj = SAPEmployeeInfoList[0];
                                _SAPCustomerQuoteObject.SalesRep_FamilyName = _SAPEmployeeInfoObj.FamilyName;
                                _SAPCustomerQuoteObject.SalesRep_GivenName = _SAPEmployeeInfoObj.GivenName;
                                if (_SAPEmployeeInfoObj.EmailAddress == null)
                                {
                                    ContactInfo _BusinessPartnersInfoObj = QueryBusinessPartners.GetBusinessPartnersInfoFromSAP(_SAPCustomerQuoteObject.SalesRep_FamilyName, _SAPCustomerQuoteObject.SalesRep_GivenName);
                                    _SAPCustomerQuoteObject.SalesRep_EmailAddress = _BusinessPartnersInfoObj.EmailAddress;
                                }
                                else
                                    _SAPCustomerQuoteObject.SalesRep_EmailAddress = _SAPEmployeeInfoObj.EmailAddress;

                            }
                        }
                    }
                }
                return _SAPCustomerQuoteObject;
            }
            return null;
        }

        private SAPQueryCustomerQuoteIn.CustomerQuoteByElementsResponseMessage_sync CallWebService(String SalesQuoteID)
        {
            //Create the Web Service reference service object
            SAPQueryCustomerQuoteIn.service svc = new SAPQueryCustomerQuoteIn.service();
            //Set Credentials

            String uName = Globals.ThisAddIn.Username;
            String uPassword = Globals.ThisAddIn.Password;

            Globals.ThisAddIn.Username = AppConstants.InternalSAPUserName;
            Globals.ThisAddIn.Password = AppConstants.InternalSAPPassword;
            svc.Credentials = new SAPCredentials();


            //Create Request object
            SAPQueryCustomerQuoteIn.CustomerQuoteByElementsQueryMessage_sync request = new SAPQueryCustomerQuoteIn.CustomerQuoteByElementsQueryMessage_sync();

            request.CustomerQuoteSelectionByElements = new SAPQueryCustomerQuoteIn.CustomerQuoteByElementsQuerySelectionByElements();

            request.CustomerQuoteSelectionByElements.SelectionByID = new SAPQueryCustomerQuoteIn.CustomerQuoteByElementsQuerySelectionByID[1];
            request.CustomerQuoteSelectionByElements.SelectionByID[0] = new SAPQueryCustomerQuoteIn.CustomerQuoteByElementsQuerySelectionByID();

            request.CustomerQuoteSelectionByElements.SelectionByID[0].InclusionExclusionCode = "I";
            request.CustomerQuoteSelectionByElements.SelectionByID[0].IntervalBoundaryTypeCode = "1";
            request.CustomerQuoteSelectionByElements.SelectionByID[0].LowerBoundaryID = new SAPQueryCustomerQuoteIn.BusinessTransactionDocumentID();
            request.CustomerQuoteSelectionByElements.SelectionByID[0].LowerBoundaryID.Value = SalesQuoteID;

            request.ProcessingConditions = new SAPQueryCustomerQuoteIn.QueryProcessingConditions();
            request.ProcessingConditions.QueryHitsMaximumNumberValue = 0;
            request.ProcessingConditions.QueryHitsUnlimitedIndicator = true;
            request.ProcessingConditions.QueryHitsMaximumNumberValueSpecified = true;

            try
            {
                //Make the call, passing the request
                SAPQueryCustomerQuoteIn.CustomerQuoteByElementsResponseMessage_sync response = svc.FindByElements(request);
                Globals.ThisAddIn.Username = uName;
                Globals.ThisAddIn.Password = uPassword;
                return response;
            }
            catch (Exception exp)
            {
                Globals.ThisAddIn.Username = uName;
                Globals.ThisAddIn.Password = uPassword;
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
            }
            return null;
        }
    }

    public class SAPCustomerQuote
    {
        public String ID;
        public DateTime PostingDate;
        public String EmployeeResponsiblePartyID;
        public String EmployeeResponsiblePartyEmail;
        public String SalesCycle;
        public String Source;
        public String SalesPhase;
        public String ChanceofSuccess;
        public String SalesRep;
        public String SalesRep_FamilyName;
        public String SalesRep_GivenName;
        public String SalesRep_EmailAddress;
    }
}
