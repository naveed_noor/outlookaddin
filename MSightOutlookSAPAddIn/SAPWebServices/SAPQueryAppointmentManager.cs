﻿using MSightOutlookSAPAddIn.UtilClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSightOutlookSAPAddIn.SAPWebServices
{

    public class SAPQueryAppointmentManager
    {
        public static List<SAPQueryAppointment> QueryAppointmentsFromSAP(String _EmployeeResponsibleInternalID, DateTime _StartDateTime, DateTime _EndDateTime)
        {
            List<SAPQueryAppointment> SAPQueryAppointmentList = new List<SAPQueryAppointment>();
            try
            {
                String EmployeeResponsibleInternalID = _EmployeeResponsibleInternalID;
                DateTime StartDateTime = DateTime.Parse(_StartDateTime.ToString("yyyy-MM-ddT00:00:00.000000Z")).ToUniversalTime();
                DateTime EndDateTime = DateTime.Parse(_EndDateTime.ToString("yyyy-MM-ddT23:59:59.999999Z")).ToUniversalTime();

                SAPQueryAppointments.AppointmentActivityQueryByElementsSimpleByConfirmationMessage_sync response = CallQueryByElementWebService(EmployeeResponsibleInternalID, StartDateTime, EndDateTime);
                if (response != null && response.AppointmentActivity != null && response.AppointmentActivity.Length > 0)
                {
                    foreach (var AppointmentActivityObj in response.AppointmentActivity)
                    {
                        String SAPAppointmentID = AppointmentActivityObj.ID.Value;
                        String SAPAppointmentUUID = AppointmentActivityObj.UUID.Value;
                        SAPQueryAppointments.AppointmentActivityReadByIDResponseMessage_sync ReadResponse = CallReadAppointmentActivityWebService(SAPAppointmentID, SAPAppointmentUUID);
                        if (ReadResponse != null && ReadResponse.AppointmentActivity != null)
                        {
                            SAPQueryAppointment _SAPQueryAppointmentObjectObj = new SAPQueryAppointment();
                            // Initialize Struct List to add Parties
                            _SAPQueryAppointmentObjectObj.SAPQueryAppointmentPartyList = new List<SAPQueryAppointmentParty>();

                            _SAPQueryAppointmentObjectObj.ID = ReadResponse.AppointmentActivity.ID.Value;
                            _SAPQueryAppointmentObjectObj.UUID = ReadResponse.AppointmentActivity.UUID.Value;
                            _SAPQueryAppointmentObjectObj.Name = ReadResponse.AppointmentActivity.Name.Value;
                            _SAPQueryAppointmentObjectObj.PriorityName = ReadResponse.AppointmentActivity.PriorityName;
                            _SAPQueryAppointmentObjectObj.GroupName = ReadResponse.AppointmentActivity.GroupName;
                            _SAPQueryAppointmentObjectObj.LifeCycleStatusName = ReadResponse.AppointmentActivity.Status.LifeCycleStatusName;

                            _SAPQueryAppointmentObjectObj.LastChangeIdentityID = ReadResponse.AppointmentActivity.LastChangeIdentity.ID.Value;
                            _SAPQueryAppointmentObjectObj.LastChangeIdentityName = ReadResponse.AppointmentActivity.LastChangeIdentity.FormattedName;

                            _SAPQueryAppointmentObjectObj.CreationIdentityID = ReadResponse.AppointmentActivity.CreationIdentity.ID.Value;
                            _SAPQueryAppointmentObjectObj.CreationIdentityName = ReadResponse.AppointmentActivity.CreationIdentity.FormattedName;

                            if (ReadResponse.AppointmentActivity.TextCollection != null &&
                                    ReadResponse.AppointmentActivity.TextCollection.Text != null && ReadResponse.AppointmentActivity.TextCollection.Text.Length > 0 &&
                                    ReadResponse.AppointmentActivity.TextCollection.Text[0].TextContent != null &&
                                ReadResponse.AppointmentActivity.TextCollection.Text[0].TypeCode.Value.Equals("10002"))
                            {
                                if (ReadResponse.AppointmentActivity.TextCollection.Text[0].TextContent != null && 
                                        ReadResponse.AppointmentActivity.TextCollection.Text[0].TextContent.Text != null)                                   
                                        _SAPQueryAppointmentObjectObj.Notes = ReadResponse.AppointmentActivity.TextCollection.Text[0].TextContent.Text.Value;
                            }

                            if (ReadResponse.AppointmentActivity.ScheduledPeriod != null && ReadResponse.AppointmentActivity.ScheduledPeriod.TimePointPeriod != null &&
                                ReadResponse.AppointmentActivity.ScheduledPeriod.TimePointPeriod.StartTimePoint != null &&
                                ReadResponse.AppointmentActivity.ScheduledPeriod.TimePointPeriod.EndTimePoint != null)
                            {
                                _SAPQueryAppointmentObjectObj.ScheduledPeriod_StartDateTime = ReadResponse.AppointmentActivity.ScheduledPeriod.TimePointPeriod.StartTimePoint.DateTime.Value;
                                _SAPQueryAppointmentObjectObj.ScheduledPeriod_EndDateTime = ReadResponse.AppointmentActivity.ScheduledPeriod.TimePointPeriod.EndTimePoint.DateTime.Value;

                                if (ReadResponse.AppointmentActivity.ScheduledPeriod.FullDayIndicatorSpecified == true)
                                    _SAPQueryAppointmentObjectObj.FullDayIndicator = ReadResponse.AppointmentActivity.ScheduledPeriod.FullDayIndicator;
                            }

                            if (ReadResponse.AppointmentActivity.OtherBusinessTransactionDocumentReference != null &&
                                    ReadResponse.AppointmentActivity.OtherBusinessTransactionDocumentReference.Length > 0 &&
                                    ReadResponse.AppointmentActivity.OtherBusinessTransactionDocumentReference[0].BusinessTransactionDocumentReference != null &&
                                    ReadResponse.AppointmentActivity.OtherBusinessTransactionDocumentReference[0].BusinessTransactionDocumentReference.ID != null)
                            {
                                _SAPQueryAppointmentObjectObj.AppointmentID = ReadResponse.AppointmentActivity.OtherBusinessTransactionDocumentReference[0].BusinessTransactionDocumentReference.ID.Value;
                            }

                            if (ReadResponse.AppointmentActivity.MainLocation != null && ReadResponse.AppointmentActivity.MainLocation.Name != null)
                                _SAPQueryAppointmentObjectObj.MeetingPlace = ReadResponse.AppointmentActivity.MainLocation.Name.Value;

                            foreach (var party in ReadResponse.AppointmentActivity.Party)
                            {
                                SAPQueryAppointmentParty _SAPQueryAppointmentPartyObj = new SAPQueryAppointmentParty();
                                if (party.RoleName.Equals("Organizer"))
                                    _SAPQueryAppointmentPartyObj.PartyType = PartyType.Organizer;
                                else if (party.RoleName.Equals("Employee Responsible"))
                                    _SAPQueryAppointmentPartyObj.PartyType = PartyType.EmployeeResponsible;
                                else if (party.RoleName.Equals("Account"))
                                    _SAPQueryAppointmentPartyObj.PartyType = PartyType.Account;
                                else if (party.RoleName.Equals("Attendee"))
                                    _SAPQueryAppointmentPartyObj.PartyType = PartyType.Attendee;

                                _SAPQueryAppointmentPartyObj.PartyID = party.PartyKey.PartyID.Value;
                                _SAPQueryAppointmentPartyObj.PartyTypeName = party.PartyKey.PartyTypeName;
                                _SAPQueryAppointmentPartyObj.PartyTypeCode = party.PartyKey.PartyTypeCode.Value;
                                _SAPQueryAppointmentPartyObj.PartyUUID = party.PartyUUID.Value;

                                _SAPQueryAppointmentPartyObj.PartyContactInfo = new ContactInfo();

                                BusinessParnerCategoryCode BPCategoryCode = BusinessParnerCategoryCode.NotKnown;
                                if (_SAPQueryAppointmentPartyObj.PartyTypeCode.Equals("147")) // In Case of Vendor
                                    BPCategoryCode = BusinessParnerCategoryCode.BusniessParnner;
                                else if (_SAPQueryAppointmentPartyObj.PartyTypeCode.Equals("167")) // In Case of Employee Responsible 
                                    BPCategoryCode = BusinessParnerCategoryCode.Employee;

                                List<ContactInfo> BusinessPartnersInfoList = QueryBusinessPartners.GetBusinessPartnersInfoFromSAP(_SAPQueryAppointmentPartyObj.PartyID, BPCategoryCode);

                                if (BusinessPartnersInfoList != null && BusinessPartnersInfoList.Count > 0)
                                {
                                    foreach (ContactInfo BusinessPartnersInfoItem in BusinessPartnersInfoList)
                                    {
                                        _SAPQueryAppointmentPartyObj.PartyContactInfo = BusinessPartnersInfoItem;
                                    }
                                }

                                _SAPQueryAppointmentObjectObj.SAPQueryAppointmentPartyList.Add(_SAPQueryAppointmentPartyObj);
                            }

                            SAPQueryAppointmentList.Add(_SAPQueryAppointmentObjectObj);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("The following error occurred while Querying Appointments from SAP: " + ex.Message);
            }
            return SAPQueryAppointmentList;
        }

        private static SAPQueryAppointments.AppointmentActivityReadByIDResponseMessage_sync CallReadAppointmentActivityWebService(String SAPAppointmentID, String SAPAppointmentUUID)
        {
            //Create the Web Service reference service object
            SAPQueryAppointments.service svc = new SAPQueryAppointments.service();
            //Set Credentials

            String uName = Globals.ThisAddIn.Username;
            String uPassword = Globals.ThisAddIn.Password;

            Globals.ThisAddIn.Username = AppConstants.InternalSAPUserName;
            Globals.ThisAddIn.Password = AppConstants.InternalSAPPassword;
            svc.Credentials = new SAPCredentials();

            SAPQueryAppointments.AppointmentActivityReadByIDQueryMessage_sync request = new SAPQueryAppointments.AppointmentActivityReadByIDQueryMessage_sync();

            request.AppointmentActivity = new SAPQueryAppointments.AppointmentActivityReadByIDQuery();
            request.AppointmentActivity.ID = new SAPQueryAppointments.BusinessTransactionDocumentID();
            request.AppointmentActivity.ID.Value = SAPAppointmentID;
            request.AppointmentActivity.UUID = new SAPQueryAppointments.UUID();
            request.AppointmentActivity.UUID.Value = SAPAppointmentUUID;
            
            try
            {
                //Make the call, passing the request
                SAPQueryAppointments.AppointmentActivityReadByIDResponseMessage_sync response = svc.Read(request);
                Globals.ThisAddIn.Username = uName;
                Globals.ThisAddIn.Password = uPassword;
                return response;
            }
            catch (Exception exp)
            {
                Globals.ThisAddIn.Username = uName;
                Globals.ThisAddIn.Password = uPassword;
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
            }
            return null;
        }

        private static SAPQueryAppointments.AppointmentActivityQueryByElementsSimpleByConfirmationMessage_sync CallQueryByElementWebService(String EmployeeResponsibleInternalID, DateTime StartDateTime, DateTime EndDateTime)
        {
            //Create the Web Service reference service object
            SAPQueryAppointments.service svc = new SAPQueryAppointments.service();
            //Set Credentials

            String uName = Globals.ThisAddIn.Username;
            String uPassword = Globals.ThisAddIn.Password;

            Globals.ThisAddIn.Username = AppConstants.InternalSAPUserName;
            Globals.ThisAddIn.Password = AppConstants.InternalSAPPassword;
            svc.Credentials = new SAPCredentials();

            //Create Request object
            SAPQueryAppointments.AppointmentActivityQueryByElementsSimpleByRequestMessage_sync request = new SAPQueryAppointments.AppointmentActivityQueryByElementsSimpleByRequestMessage_sync();

            request.AppointmentActivitySimpleSelectionBy = new SAPQueryAppointments.AppointmentActivityQueryByElementsSimpleByRequest();

            request.AppointmentActivitySimpleSelectionBy.SelectionBySystemAdministrativeDataCreationDateTime = new SAPQueryAppointments.AppointmentActivityQueryByElementsSimpleByRequestSelectionBySystemAdministrativeDataCreationDateTime[1];
            request.AppointmentActivitySimpleSelectionBy.SelectionBySystemAdministrativeDataCreationDateTime[0] = new SAPQueryAppointments.AppointmentActivityQueryByElementsSimpleByRequestSelectionBySystemAdministrativeDataCreationDateTime();

            request.AppointmentActivitySimpleSelectionBy.SelectionBySystemAdministrativeDataCreationDateTime[0].InclusionExclusionCode = "I";
            request.AppointmentActivitySimpleSelectionBy.SelectionBySystemAdministrativeDataCreationDateTime[0].IntervalBoundaryTypeCode = "3";
            request.AppointmentActivitySimpleSelectionBy.SelectionBySystemAdministrativeDataCreationDateTime[0].LowerBoundaryCreationDateTime = StartDateTime;
            request.AppointmentActivitySimpleSelectionBy.SelectionBySystemAdministrativeDataCreationDateTime[0].LowerBoundaryCreationDateTimeSpecified = true;
            request.AppointmentActivitySimpleSelectionBy.SelectionBySystemAdministrativeDataCreationDateTime[0].UpperBoundaryCreationDateTime = EndDateTime;
            request.AppointmentActivitySimpleSelectionBy.SelectionBySystemAdministrativeDataCreationDateTime[0].UpperBoundaryCreationDateTimeSpecified = true;

            request.AppointmentActivitySimpleSelectionBy.SelectionByBusinessTransactionDocumentReferenceBusinessTransactionDocumentReferenceTypeCode = new SAPQueryAppointments.AppointmentActivityQueryByElementsSimpleByRequestSelectionByBusinessTransactionDocumentReferenceBusinessTransactionDocmn[1];

            request.AppointmentActivitySimpleSelectionBy.SelectionByBusinessTransactionDocumentReferenceBusinessTransactionDocumentReferenceTypeCode[0] = new SAPQueryAppointments.AppointmentActivityQueryByElementsSimpleByRequestSelectionByBusinessTransactionDocumentReferenceBusinessTransactionDocmn();

            request.AppointmentActivitySimpleSelectionBy.SelectionByBusinessTransactionDocumentReferenceBusinessTransactionDocumentReferenceTypeCode[0].InclusionExclusionCode = "I";
            request.AppointmentActivitySimpleSelectionBy.SelectionByBusinessTransactionDocumentReferenceBusinessTransactionDocumentReferenceTypeCode[0].IntervalBoundaryTypeCode = "1";
            request.AppointmentActivitySimpleSelectionBy.SelectionByBusinessTransactionDocumentReferenceBusinessTransactionDocumentReferenceTypeCode[0].LowerBoundaryBusinessTransactionDocumentReferenceBusinessTransactionDocumentReferenceTypeCode = new SAPQueryAppointments.BusinessTransactionDocumentTypeCode();
            request.AppointmentActivitySimpleSelectionBy.SelectionByBusinessTransactionDocumentReferenceBusinessTransactionDocumentReferenceTypeCode[0].LowerBoundaryBusinessTransactionDocumentReferenceBusinessTransactionDocumentReferenceTypeCode.Value = "30";

            /*
            request.AppointmentActivitySimpleSelectionBy.SelectionByPartyEmployeeResponsiblePartyKeyPartyID = new SAPQueryAppointments.AppointmentActivityQApmntInQueryByElementsSimpleByRequestSelectionByPartyEmployeeResponsiblePartyKeyPartyID[1];

            request.AppointmentActivitySimpleSelectionBy.SelectionByPartyEmployeeResponsiblePartyKeyPartyID[0] = new SAPQueryAppointments.AppointmentActivityQApmntInQueryByElementsSimpleByRequestSelectionByPartyEmployeeResponsiblePartyKeyPartyID();
            request.AppointmentActivitySimpleSelectionBy.SelectionByPartyEmployeeResponsiblePartyKeyPartyID[0].InclusionExclusionCode = "I";
            request.AppointmentActivitySimpleSelectionBy.SelectionByPartyEmployeeResponsiblePartyKeyPartyID[0].IntervalBoundaryTypeCode = "1";
            request.AppointmentActivitySimpleSelectionBy.SelectionByPartyEmployeeResponsiblePartyKeyPartyID[0].LowerBoundaryPartyID = new SAPQueryAppointments.PartyID();
            request.AppointmentActivitySimpleSelectionBy.SelectionByPartyEmployeeResponsiblePartyKeyPartyID[0].LowerBoundaryPartyID.Value = EmployeeResponsibleInternalID;
            */

            request.AppointmentActivitySimpleSelectionBy.SelectionByStatusLifeCycleStatusCode = new SAPQueryAppointments.AppointmentActivityQueryByElementsSimpleByRequestSelectionByStatusLifeCycleStatusCode[1];

            request.AppointmentActivitySimpleSelectionBy.SelectionByStatusLifeCycleStatusCode[0] = new SAPQueryAppointments.AppointmentActivityQueryByElementsSimpleByRequestSelectionByStatusLifeCycleStatusCode();
            request.AppointmentActivitySimpleSelectionBy.SelectionByStatusLifeCycleStatusCode[0].InclusionExclusionCode = "I";
            request.AppointmentActivitySimpleSelectionBy.SelectionByStatusLifeCycleStatusCode[0].IntervalBoundaryTypeCode = "3";
            request.AppointmentActivitySimpleSelectionBy.SelectionByStatusLifeCycleStatusCode[0].LowerBoundaryLifeCycleStatusCode = SAPQueryAppointments.ActivityLifeCycleStatusCode.Item1;
            request.AppointmentActivitySimpleSelectionBy.SelectionByStatusLifeCycleStatusCode[0].UpperBoundaryLifeCycleStatusCode = SAPQueryAppointments.ActivityLifeCycleStatusCode.Item2;
            request.AppointmentActivitySimpleSelectionBy.SelectionByStatusLifeCycleStatusCode[0].LowerBoundaryLifeCycleStatusCodeSpecified = true;
            request.AppointmentActivitySimpleSelectionBy.SelectionByStatusLifeCycleStatusCode[0].UpperBoundaryLifeCycleStatusCodeSpecified = true;

            request.ProcessingConditions = new SAPQueryAppointments.QueryProcessingConditions();
            request.ProcessingConditions.QueryHitsMaximumNumberValue = 0;
            request.ProcessingConditions.QueryHitsUnlimitedIndicator = true;
            request.ProcessingConditions.QueryHitsMaximumNumberValueSpecified = true;

            try
            {
                //Make the call, passing the request
                SAPQueryAppointments.AppointmentActivityQueryByElementsSimpleByConfirmationMessage_sync response = svc.QueryByElements(request);
                Globals.ThisAddIn.Username = uName;
                Globals.ThisAddIn.Password = uPassword;
                return response;
            }
            catch (Exception exp)
            {
                Globals.ThisAddIn.Username = uName;
                Globals.ThisAddIn.Password = uPassword;
                if (exp.Message.ToString().Contains("The remote name could not be resolved") == true)
                {
                    throw new Exception("Network Issue and Can't reach to the SAP ByDesign Server");
                }
                else
                    MessageBox.Show("Error Occured while Querying Appointments from SAP. Error Detail is : " + exp.Message);
            }
            return null;
        }

    }
}
